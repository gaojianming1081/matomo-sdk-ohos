## 0.0.2-SNAPSHOT
* optimization: Optimize code

## 0.0.1-SNAPSHOT
* refactor: For module library Android-to-OpenHarmony

## v4.1.2

Reduced impact
* Reduced impact on app performance (Thanks @Tolriq)

## v4.1.1

* Fixed disk caching loosing events, see #271

## v4.1.0

Improved edge-case behavior
* Align user-id / visitor-id behavior with other SDKs. (#256)
* Fix UncaughtExceptionHandler loosing events on crash (#252)

## v4.0.2

Bugfix
* Fixes #243 which lead to price not being tracked for larger prices

## v4.0.1

Improved debugging
* Better log tags

## v4.0.0

Piwik -> Matomo
* Major refactoring from Piwik to Matomo (#191), see here for migration hints
* Additional debugging information (#228)
* A Tracker callback to allow extensions that modify the tracked data shortly before sending, e.g. for custom dimensions (#224)
* Removed any pattern enforcing on the application domain (#217)

##  v3.0.4

Bugfixes
* Fixes a potential connection leak (#226)

## v3.0.3

Bugfixes
* The Android SDK did not second the necessary information to allow for server-side stitching when changing the user-id within a sesion (#209)
* Fixed a bug causing the session to not time out due to start-time being updated too often (#210)
* Updated used android supportlibs
* Switched the demo app to https and matomo.org

## v3.0.2

* Fixed issue with transitive dependencies on Timber and SupportAnnotations leaking through (#204, #184, #207)
* Updated buildtools / gradle

## v3.0.1

Bugfix release
* Fixed: If the server returns a non-OK (non 2XX status code), the response can contain an error stream. This error stream can leak an open socket if not consumed (or at least explicitly closed). See #199.
* Fixed: When using GZIP & POST the OutputStream could leak resources if it is not explicitly closed.
* Improved: Failure to close an output stream AFTER data transmission (without exception) is now no longer failing the send(Packet) call.
