package com.org.matomo.demo.chechbox;
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import static ohos.agp.components.ComponentState.*;

/**
 * BaseHelper
 *
 * @author ZhongDaFeng
 */
public class RBaseHelper<T extends Component> implements IClip, Component.LayoutRefreshedListener, Component.DrawTask, Component.TouchEventListener {
    /**
     * 自定义背景key
     */
    private static final String background_normal = "background_normal";
    private static final String background_pressed = "background_pressed";
    private static final String background_unable = "background_unable";
    private static final String background_checked = "background_checked";
    private static final String background_selected = "background_selected";

    /**
     * 渐变方向
     */
    private static final String TOP_BOTTOM = "TOP_BOTTOM";
    private static final String TR_BL = "TR_BL";
    private static final String RIGHT_LEFT = "RIGHT_LEFT";
    private static final String BR_TL = "BR_TL";
    private static final String BOTTOM_TOP = "BOTTOM_TOP";
    private static final String BL_TR = "BL_TR";
    private static final String LEFT_RIGHT = "LEFT_RIGHT";
    private static final String TL_BR = "TL_BR";
    private static final String gradient_orientation = "gradient_orientation";
    private static final String corner_radius = "corner_radius";
    private static final String corner_radius_top_left = "corner_radius_top_left";
    private static final String corner_radius_top_right = "corner_radius_top_right";
    private static final String corner_radius_bottom_left = "corner_radius_bottom_left";
    private static final String corner_radius_bottom_right = "corner_radius_bottom_right";

    /**
     * 边框宽度
     */
    private static final String border_width_normal = "border_width_normal";
    private static final String border_width_pressed = "border_width_pressed";
    private static final String border_width_unable = "border_width_unable";
    private static final String border_width_checked = "border_width_checked";
    private static final String border_width_selected = "border_width_selected";

    /**
     * 边框颜色
     */
    private static final String border_color_normal = "border_color_normal";
    private static final String border_color_pressed = "border_color_pressed";
    private static final String border_color_unable = "border_color_unable";
    private static final String border_color_checked = "border_color_checked";
    private static final String border_color_selected = "border_color_selected";
    private String strBgType = "";

    /**
     * 背景类型{1:单一颜色值 2:颜色数组 3:图片}
     */
    protected int BG_TYPE_COLOR = 1, BG_TYPE_COLOR_ARRAY = 2, BG_TYPE_IMG = 3;

    //corner
    private float mCornerRadius;

    private float mCornerRadiusTopLeft;
    private float mCornerRadiusTopRight;
    private float mCornerRadiusBottomLeft;
    private float mCornerRadiusBottomRight;

    //BorderWidth
    private float mBorderDashWidth = 0;
    private float mBorderDashGap = 0;
    private int mBorderWidthNormal = 0;
    private int mBorderWidthPressed = 0;
    private int mBorderWidthUnable = 0;
    private int mBorderWidthChecked = 0;
    private int mBorderWidthSelected = 0;

    //BorderColor
    private Color mBorderColorNormal;
    private Color mBorderColorPressed;
    private Color mBorderColorUnable;
    private Color mBorderColorChecked;
    private Color mBorderColorSelected;

    //Background
    private Color mBackgroundColorNormal;
    private Color mBackgroundColorPressed;
    private Color mBackgroundColorUnable;
    private Color mBackgroundColorChecked;
    private Color mBackgroundColorSelected;
    //BackgroundColorArray
    private Color[] mBackgroundColorNormalArray;
    private Color[] mBackgroundColorPressedArray;
    private Color[] mBackgroundColorUnableArray;
    private Color[] mBackgroundColorCheckedArray;
    private Color[] mBackgroundColorSelectedArray;

    private ShapeElement mBackgroundNormal;
    private ShapeElement mBackgroundPressed;
    private ShapeElement mBackgroundUnable;
    private ShapeElement mBackgroundChecked;
    private ShapeElement mBackgroundSelected;
    private Element mBackgroundNormalBmp;
    private Element mBackgroundPressedBmp;
    private Element mBackgroundUnableBmp;
    private Element mBackgroundCheckedBmp;
    private Element mBackgroundSelectedBmp;

    //Gradient
    private int mGradientType = 0;
    private float mGradientRadius;
    private float mGradientCenterX, mGradientCenterY;
    private ShapeElement.Orientation mGradientOrientation = ShapeElement.Orientation.BOTTOM_TO_TOP;
    //shadow
    private ShadowDrawable mShadowDrawable;
    private int mShadowDx;
    private int mShadowDy;
    private Color mShadowColor;
    private int mShadowRadius;

    //View/ViewGroup是否可用
    private boolean mIsEnabled = true;

    //ripple
    private boolean isUseRipple;
    private Color mRippleColor;
    private Element mRippleMaskDrawable;
    //Ripple波纹限制样式{null, normal=控件矩形, drawable=自定义drawable}
    private int mRippleMaskStyle;
    //null normal drawable
    private final int MASK_STYLE_NULL = 1;

    private Element mViewBackground;//控件默认背景Drawable

    private int[][] states = new int[6][];
    private StateElement mStateBackground;
    private float[] mBorderRadii = new float[8];

    protected Context mContext;

    /**
     * 是否设置对应的属性
     */
    private boolean isHasPressedBgColor = false;
    private boolean isHasPressedBgBmp = false;
    private boolean isHasUnableBgColor = false;
    private boolean isHasUnableBgBmp = false;
    private boolean isHasCheckedBgColor = false;
    private boolean isHasSelectedBgColor = false;
    private boolean isHasCheckedBgBmp = false;
    private boolean isHasSelectedBgBmp = false;
    private boolean isHasPressedBorderColor = false;
    private boolean isHasUnableBorderColor = false;
    private boolean isHasCheckedBorderColor = false;
    private boolean isHasSelectedBorderColor = false;
    private boolean isHasPressedBorderWidth = false;
    private boolean isHasUnableBorderWidth = false;
    private boolean isHasCheckedBorderWidth = false;
    private boolean isHasSelectedBorderWidth = false;

    // view
    protected T mView;

    //EmptyStateListDrawable
    private StateElement emptyStateListDrawable = new StateElement();

    //ClipHelper
    protected ClipHelper mClipHelper = new ClipHelper();
    //clipLayout
    private boolean isClipLayout = false;

    private int sy;
    private float sx;
    AnimatorValue animatorValue = new AnimatorValue();
    private float zj;
    private Paint paint;

    public RBaseHelper(Context context, T view, AttrSet attrs) {
        mView = view;
        mContext = context;
        mView.addDrawTask(this);
        mView.setTouchEventListener(this);
        mView.setLayoutRefreshedListener(this);
        initAttributeSet(context, attrs);
        paint = new Paint();
        paint.setColor(mRippleColor);

        paint.setAlpha(0.2f);
        paint.setStyle(Paint.Style.FILL_STYLE);
        animatorValue.setDuration(1000);
        animatorValue.setCurveType(Animator.CurveType.LINEAR);
    }

    /**
     * 初始化控件属性
     *
     * @param context 上下文
     * @param attrs AttrSet对象
     */
    private void initAttributeSet(Context context, AttrSet attrs) {
        if (context == null || attrs == null) {
            setup();
            return;
        }
        mCornerRadius = AttrUtils.getInt(attrs, corner_radius, -1);
        mCornerRadiusTopLeft = AttrUtils.getInt(attrs, corner_radius_top_left, 0);
        mCornerRadiusTopRight = AttrUtils.getInt(attrs, corner_radius_top_right, 0);
        mCornerRadiusBottomLeft = AttrUtils.getInt(attrs, corner_radius_bottom_left, 0);
        mCornerRadiusBottomRight = AttrUtils.getInt(attrs, corner_radius_bottom_right, 0);

        // border
        mBorderWidthNormal = AttrUtils.getInt(attrs, border_width_normal, 0);
        mBorderWidthPressed = AttrUtils.getInt(attrs, border_width_pressed, 0);
        mBorderWidthUnable = AttrUtils.getInt(attrs, border_width_unable, 0);
        mBorderWidthChecked = AttrUtils.getInt(attrs, border_width_checked, 0);
        mBorderWidthSelected = AttrUtils.getInt(attrs, border_width_selected, 0);
        mBorderDashGap = AttrUtils.getFloat(attrs, "border_dash_gap", 0);
        mBorderDashWidth = AttrUtils.getFloat(attrs, "border_dash_width", 0);
        // border 颜色
        mBorderColorNormal = getStringColor(attrs, border_color_normal, null);
        mBorderColorPressed = getStringColor(attrs, border_color_pressed, null);
        mBorderColorUnable = getStringColor(attrs, border_color_unable, null);
        mBorderColorChecked =getStringColor(attrs, border_color_checked, null);
        mBorderColorSelected = getStringColor(attrs, border_color_selected, null);
        strBgType = AttrUtils.getXmlManyValue(attrs, "bgType", "color");

        Object[] bgInfoNormal;
        bgInfoNormal = getBackgroundInfo(attrs, background_normal);
        mBackgroundColorNormal = (Color) bgInfoNormal[1];
        mBackgroundColorNormalArray = (Color[]) bgInfoNormal[2];
        mBackgroundNormalBmp = (Element) bgInfoNormal[3];

        //pressed
        Object[] bgInfoPressed = getBackgroundInfo(attrs, background_pressed);
        mBackgroundColorPressed = (Color) bgInfoPressed[1];
        mBackgroundColorPressedArray = (Color[]) bgInfoPressed[2];
        mBackgroundPressedBmp = (Element) bgInfoPressed[3];
        //unable
        Object[] bgInfoUnable = getBackgroundInfo(attrs, background_unable);
        mBackgroundColorUnable = (Color) bgInfoUnable[1];
        mBackgroundColorUnableArray = (Color[]) bgInfoUnable[2];
        mBackgroundUnableBmp = (Element) bgInfoUnable[3];
        //checked
        Object[] bgInfoChecked = getBackgroundInfo(attrs, background_checked);
        mBackgroundColorChecked = (Color) bgInfoChecked[1];
        mBackgroundColorCheckedArray = (Color[]) bgInfoChecked[2];
        mBackgroundCheckedBmp = (Element) bgInfoChecked[3];
        //selected
        Object[] bgInfoSelected = getBackgroundInfo(attrs, background_selected);
        mBackgroundColorSelected = (Color) bgInfoSelected[1];
        mBackgroundColorSelectedArray = (Color[]) bgInfoSelected[2];
        mBackgroundSelectedBmp = (Element) bgInfoSelected[3];
        //gradient
        String gradient_type = "gradient_type";
        mGradientType = AttrUtils.getInt(attrs, gradient_type, 0);
        mGradientOrientation = getGradientOrientation(attrs);

        String gradient_radius = "gradient_radius";
        mGradientRadius = AttrUtils.getFloat(attrs, gradient_radius, -1);
        String gradient_centerX = "gradient_centerX";
        mGradientCenterX = AttrUtils.getFloat(attrs, gradient_centerX, 0.5f);
        String gradient_centerY = "gradient_centerY";
        mGradientCenterY = AttrUtils.getFloat(attrs, gradient_centerY, 0.5f);

        //enabled
        String enabled = "enabled";
        mIsEnabled = getBool(attrs, enabled, "true");

        //Ripple
        try {
            isUseRipple = getBool(attrs, "ripple", "false");
            mRippleColor = getStringColor(attrs, "ripple_color", null);
            mRippleMaskDrawable = AttrUtils.getElement(attrs, "ripple_mask", null);
            int MASK_STYLE_NORMAL = 2;
            mRippleMaskStyle = AttrUtils.getInt(attrs, "ripple_mask_style", MASK_STYLE_NORMAL);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //shadow
        mShadowDx = AttrUtils.getInt(attrs, "shadow_dx", 0);
        mShadowDy = AttrUtils.getInt(attrs, "shadow_dy", 0);
        mShadowColor = getStringColor(attrs, "shadow_color", null);
        mShadowRadius = AttrUtils.getInt(attrs, "shadow_radius", -1);
        //clip
        isClipLayout = getBool(attrs, "clip_layout", "false");

        isHasPressedBgColor = mBackgroundColorPressed != null || mBackgroundColorPressedArray != null;
        isHasUnableBgColor = mBackgroundColorUnable != null || mBackgroundColorUnableArray != null;
        isHasCheckedBgColor = mBackgroundColorChecked != null || mBackgroundColorCheckedArray != null;
        isHasSelectedBgColor = mBackgroundColorSelected != null || mBackgroundColorSelectedArray != null;
        isHasPressedBgBmp = mBackgroundPressedBmp != null;
        isHasUnableBgBmp = mBackgroundUnableBmp != null;
        isHasCheckedBgBmp = mBackgroundCheckedBmp != null;
        isHasSelectedBgBmp = mBackgroundSelectedBmp != null;

        isHasPressedBorderColor = mBorderColorPressed != null;

        isHasUnableBorderColor = mBorderColorUnable != null;
        isHasCheckedBorderColor = mBorderColorChecked != null;
        isHasSelectedBorderColor = mBorderColorSelected != null;
        isHasPressedBorderWidth = mBorderWidthPressed != 0;
        isHasUnableBorderWidth = mBorderWidthUnable != 0;
        isHasCheckedBorderWidth = mBorderWidthChecked != 0;
        isHasSelectedBorderWidth = mBorderWidthSelected != 0;
        setup();
    }

    private boolean getBool(AttrSet attrs, String enabled, String s) {
        String bool = AttrUtils.getString(attrs, enabled, s);
        return bool.toLowerCase().equals("true");
    }

    private Color getStringColor(AttrSet attrs, String colorKey, String s) {
        String color = AttrUtils.getString(attrs, colorKey, s);
        if (color == null) {
            return Color.TRANSPARENT;
        }
        return new Color(Color.getIntColor(color));
    }

    /**
     * 设置
     */
    private void setup() {
        boolean isEnabled = mView.isEnabled();
        if (isEnabled) mView.setEnabled(mIsEnabled);

        mBackgroundNormal = new ShapeElement();
        mBackgroundPressed = new ShapeElement();
        mBackgroundUnable = new ShapeElement();
        mBackgroundChecked = new ShapeElement();
        mBackgroundSelected = new ShapeElement();
        mViewBackground = mView.getBackgroundElement();
        mStateBackground = new StateElement();
        if (!isHasPressedBgColor) {
            mBackgroundColorPressed = isHasCheckedBgColor ? mBackgroundColorChecked : mBackgroundColorNormal;
            mBackgroundColorPressedArray = isHasCheckedBgColor ? mBackgroundColorCheckedArray : mBackgroundColorNormalArray;
        }
        if (!isHasPressedBgBmp) {
            mBackgroundPressedBmp = isHasCheckedBgBmp ? mBackgroundCheckedBmp : mBackgroundNormalBmp;
        }
        if (!isHasUnableBgColor) {
            mBackgroundColorUnable = mBackgroundColorNormal;
            mBackgroundColorUnableArray = mBackgroundColorNormalArray;
        }
        if (!isHasUnableBgBmp) {
            mBackgroundUnableBmp = mBackgroundNormalBmp;
        }
        if (!isHasCheckedBgColor) {
            mBackgroundColorChecked = mBackgroundColorNormal;
            mBackgroundColorCheckedArray = mBackgroundColorNormalArray;
        }
        if (!isHasSelectedBgColor) {
            mBackgroundColorSelected = mBackgroundColorNormal;
            mBackgroundColorSelectedArray = mBackgroundColorNormalArray;
        }
        if (!isHasCheckedBgBmp) {
            mBackgroundCheckedBmp = mBackgroundNormalBmp;
        }
        if (!isHasSelectedBgBmp) {
            mBackgroundSelectedBmp = mBackgroundNormalBmp;
        }
        if (mBackgroundColorNormalArray != null && mBackgroundColorNormalArray.length > 0) {
            mBackgroundNormal = setColors(mBackgroundNormal, ColorArrayToIntArray(mBackgroundColorNormalArray));
        } else {
            if (mBackgroundColorNormal != null) {
                mBackgroundNormal.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorNormal.getValue()));
            }
        }
        if (mBackgroundColorPressedArray != null && mBackgroundColorPressedArray.length > 0) {
            mBackgroundPressed = setColors(mBackgroundPressed, ColorArrayToIntArray(mBackgroundColorPressedArray));
        } else {
            if (mBackgroundColorPressed != null) {
                mBackgroundPressed.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorPressed.getValue()));
            }
        }
        if (mBackgroundColorUnableArray != null && mBackgroundColorUnableArray.length > 0) {
            mBackgroundUnable = setColors(mBackgroundUnable, ColorArrayToIntArray(mBackgroundColorUnableArray));
        } else {
            if (mBackgroundColorUnable != null) {
                mBackgroundUnable.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorUnable.getValue()));
            }
        }
        if (mBackgroundColorCheckedArray != null && mBackgroundColorCheckedArray.length > 0) {
            mBackgroundChecked = setColors(mBackgroundChecked, ColorArrayToIntArray(mBackgroundColorCheckedArray));
        } else {
            if (mBackgroundColorChecked != null) {
                mBackgroundChecked.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorChecked.getValue()));
            }
        }
        if (mBackgroundColorSelectedArray != null && mBackgroundColorSelectedArray.length > 0) {
            mBackgroundSelected = setColors(mBackgroundSelected, ColorArrayToIntArray(mBackgroundColorSelectedArray));
        } else {
            if (mBackgroundColorSelected != null) {
                mBackgroundSelected.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorSelected.getValue()));
            }
        }
        setGradient();
        states[0] = new int[]{COMPONENT_STATE_DISABLED};//unable
        states[1] = new int[]{COMPONENT_STATE_FOCUSED};//focused
        states[2] = new int[]{COMPONENT_STATE_PRESSED};//pressed
        states[3] = new int[]{COMPONENT_STATE_CHECKED};//checked
        states[4] = new int[]{COMPONENT_STATE_SELECTED};//selected
        states[5] = new int[]{COMPONENT_STATE_EMPTY};//normal
        mStateBackground.addState(states[0], mBackgroundUnableBmp == null ? mBackgroundUnable : mBackgroundUnableBmp);
        mStateBackground.addState(states[1], mBackgroundPressedBmp == null ? mBackgroundPressed : mBackgroundPressedBmp);
        mStateBackground.addState(states[2], mBackgroundPressedBmp == null ? mBackgroundPressed : mBackgroundPressedBmp);
        mStateBackground.addState(states[3], mBackgroundCheckedBmp == null ? mBackgroundChecked : mBackgroundCheckedBmp);
        mStateBackground.addState(states[4], mBackgroundSelectedBmp == null ? mBackgroundSelected : mBackgroundSelectedBmp);
        mStateBackground.addState(states[5], mBackgroundNormalBmp == null ? mBackgroundNormal : mBackgroundNormalBmp);

        if (!isHasPressedBorderWidth) {
            mBorderWidthPressed = isHasCheckedBorderWidth ? mBorderWidthChecked : mBorderWidthNormal;
        }
        if (!isHasUnableBorderWidth) {
            mBorderWidthUnable = mBorderWidthNormal;
        }
        if (!isHasCheckedBorderWidth) {
            mBorderWidthChecked = mBorderWidthNormal;
        }
        if (!isHasSelectedBorderWidth) {
            mBorderWidthSelected = mBorderWidthNormal;
        }
        if (!isHasPressedBorderColor) {
            mBorderColorPressed = isHasCheckedBorderColor ? mBorderColorChecked : mBorderColorNormal;
        }
        if (!isHasUnableBorderColor) {
            mBorderColorUnable = mBorderColorNormal;
        }
        if (!isHasCheckedBorderColor) {
            mBorderColorChecked = mBorderColorNormal;
        }
        if (!isHasSelectedBorderColor) {
            mBorderColorSelected = mBorderColorNormal;
        }
        setBorder();
        //设置圆角
        setRadiusValue();
    }

    private int[] ColorArrayToIntArray(Color[] array) {
        int[] intArray = new int[0];
        if (array != null) {
            intArray = new int[array.length];
            for (int i = 0; i < array.length; i++) {
                intArray[i] = array[i].getValue();
            }
            return intArray;
        } else {
            return intArray;
        }
    }

    public float getGradientRadius() {
        return mGradientRadius;
    }

    public float getGradientCenterX() {
        return mGradientCenterX;
    }

    public float getGradientCenterY() {
        return mGradientCenterY;
    }

    public int getGradientType() {
        return mGradientType;
    }

    public RBaseHelper setGradientRadius(float gradientRadius) {
        this.mGradientRadius = gradientRadius;
        setGradient();
        setBackgroundState();
        return this;
    }

    public RBaseHelper setGradientCenterX(float gradientCenterX) {
        this.mGradientCenterX = gradientCenterX;
        setGradient();
        setBackgroundState();
        return this;
    }

    public RBaseHelper setGradientCenterY(float gradientCenterY) {
        this.mGradientCenterY = gradientCenterY;
        setGradient();
        setBackgroundState();
        return this;
    }

    /**
     * 设置渐变样式/类型
     *
     * @param gradientType {LINEAR_GRADIENT=0, RADIAL_GRADIENT=1, SWEEP_GRADIENT=2}
     * @return RBaseHelper对象
     */
    public RBaseHelper setGradientType(int gradientType) {
        if (gradientType < 0 || gradientType > 2) {
            gradientType = 0;
        }
        this.mGradientType = gradientType;
        setGradient();
        setBackgroundState();
        return this;
    }

    public RBaseHelper setGradientOrientation(ShapeElement.Orientation orientation) {
        this.mGradientOrientation = orientation;
        setGradient();
        setBackgroundState();
        return this;
    }

    private void setGradient() {
        mBackgroundNormal.setShaderType(mGradientType);
        mBackgroundNormal.setCornerRadius(mGradientRadius);
        mBackgroundPressed.setShaderType(mGradientType);
        mBackgroundPressed.setCornerRadius(mGradientRadius);
        mBackgroundUnable.setShaderType(mGradientType);
        mBackgroundUnable.setCornerRadius(mGradientRadius);
        mBackgroundChecked.setShaderType(mGradientType);
        mBackgroundChecked.setCornerRadius(mGradientRadius);
        mBackgroundSelected.setShaderType(mGradientType);
        mBackgroundSelected.setCornerRadius(mGradientRadius);
    }

    public RBaseHelper setStateBackgroundColor(Color normal, Color pressed, Color unable, Color checked, Color selected) {
        mBackgroundColorNormal = normal;
        mBackgroundColorPressed = pressed;
        mBackgroundColorUnable = unable;
        mBackgroundColorChecked = checked;
        mBackgroundColorSelected = selected;
        isHasPressedBgColor = true;
        isHasUnableBgColor = true;
        isHasCheckedBgColor = true;
        isHasSelectedBgColor = true;
        mBackgroundNormal.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorNormal.getValue()));
        mBackgroundPressed.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorPressed.getValue()));
        mBackgroundUnable.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorUnable.getValue()));
        mBackgroundChecked.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorChecked.getValue()));
        mBackgroundSelected.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorSelected.getValue()));
        setBackgroundState();
        return this;
    }

    public RBaseHelper setStateBackgroundColorArray(Color[] normalArray, Color[] pressedArray, Color[] unableArray, Color[] checkedArray, Color[] selectedArray) {
        mBackgroundColorNormalArray = normalArray;
        mBackgroundColorPressedArray = pressedArray;
        mBackgroundColorUnableArray = unableArray;
        mBackgroundColorCheckedArray = checkedArray;
        mBackgroundColorSelectedArray = selectedArray;
        isHasPressedBgColor = true;
        isHasUnableBgColor = true;
        isHasCheckedBgColor = true;
        isHasSelectedBgColor = true;
        mBackgroundNormal = setColors(mBackgroundNormal, ColorArrayToIntArray(mBackgroundColorNormalArray));
        mBackgroundPressed = setColors(mBackgroundPressed, ColorArrayToIntArray(mBackgroundColorPressedArray));
        mBackgroundUnable = setColors(mBackgroundUnable, ColorArrayToIntArray(mBackgroundColorUnableArray));
        mBackgroundChecked = setColors(mBackgroundChecked, ColorArrayToIntArray(mBackgroundColorCheckedArray));
        mBackgroundSelected = setColors(mBackgroundSelected, ColorArrayToIntArray(mBackgroundColorSelectedArray));
        setBorder();
        setRadiusUI();
        setGradient();
        setBackgroundState();
        return this;
    }

    public RBaseHelper setStateBackgroundColor(Element normal, Element pressed, Element unable, Element checked, Element selected) {
        mBackgroundNormalBmp = normal;
        mBackgroundPressedBmp = pressed;
        mBackgroundUnableBmp = unable;
        mBackgroundCheckedBmp = checked;
        mBackgroundSelectedBmp = selected;
        isHasPressedBgBmp = true;
        isHasUnableBgBmp = true;
        isHasCheckedBgBmp = true;
        isHasSelectedBgBmp = true;
        refreshStateListDrawable();
        setBackgroundState();
        return this;
    }

    public Color getBackgroundColorNormal() {
        return mBackgroundColorNormal;
    }

    public Color getBackgroundColorPressed() {
        return mBackgroundColorPressed;
    }

    public Color getBackgroundColorUnable() {
        return mBackgroundColorUnable;
    }

    public Color getBackgroundColorChecked() {
        return mBackgroundColorChecked;
    }

    public Color getBackgroundColorSelected() {
        return mBackgroundColorSelected;
    }

    public Color[] getBackgroundColorNormalArray() {
        return mBackgroundColorNormalArray;
    }

    public Color[] getBackgroundColorPressedArray() {
        return mBackgroundColorPressedArray;
    }

    public Color[] getBackgroundColorUnableArray() {
        return mBackgroundColorUnableArray;
    }

    public Color[] getBackgroundColorCheckedArray() {
        return mBackgroundColorCheckedArray;
    }

    public Color[] getBackgroundColorSelectedArray() {
        return mBackgroundColorSelectedArray;
    }

    public Element getBackgroundDrawableNormal() {
        return mBackgroundNormalBmp;
    }

    public Element getBackgroundDrawablePressed() {
        return mBackgroundPressedBmp;
    }

    public Element getBackgroundDrawableUnable() {
        return mBackgroundUnableBmp;
    }

    public Element getBackgroundDrawableChecked() {
        return mBackgroundCheckedBmp;
    }

    public Element getBackgroundDrawableSelected() {
        return mBackgroundSelectedBmp;
    }

    public RBaseHelper setBackgroundColorNormal(Color colorNormal) {
        this.mBackgroundColorNormal = colorNormal;
        if (!isHasPressedBgColor) {
            mBackgroundColorPressed = isHasCheckedBgColor ? mBackgroundColorChecked : mBackgroundColorNormal;
            mBackgroundPressed.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorPressed.getValue()));
        }
        if (!isHasUnableBgColor) {
            mBackgroundColorUnable = mBackgroundColorNormal;
            mBackgroundUnable.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorUnable.getValue()));
        }
        if (!isHasCheckedBgColor) {
            mBackgroundColorChecked = mBackgroundColorNormal;
            mBackgroundChecked.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorChecked.getValue()));
        }
        if (!isHasSelectedBgColor) {
            mBackgroundColorSelected = mBackgroundColorNormal;
            mBackgroundSelected.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorSelected.getValue()));
        }
        mBackgroundNormal.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorNormal.getValue()));
        setBackgroundState();
        return this;
    }

    public RBaseHelper setBackgroundColorNormalArray(Color[] colorNormalArray) {
        this.mBackgroundColorNormalArray = colorNormalArray;
        if (!isHasPressedBgColor) {
            mBackgroundColorPressedArray = isHasCheckedBgColor ? mBackgroundColorCheckedArray : mBackgroundColorNormalArray;
            mBackgroundPressed = setColors(mBackgroundPressed, ColorArrayToIntArray(mBackgroundColorPressedArray));
        }
        if (!isHasUnableBgColor) {
            mBackgroundColorUnableArray = mBackgroundColorNormalArray;
            mBackgroundUnable = setColors(mBackgroundUnable, ColorArrayToIntArray(mBackgroundColorUnableArray));
        }
        if (!isHasCheckedBgColor) {
            mBackgroundColorCheckedArray = mBackgroundColorNormalArray;
            mBackgroundChecked = setColors(mBackgroundChecked, ColorArrayToIntArray(mBackgroundColorCheckedArray));
        }
        if (!isHasSelectedBgColor) {
            mBackgroundColorSelectedArray = mBackgroundColorNormalArray;
            mBackgroundSelected = setColors(mBackgroundSelected, ColorArrayToIntArray(mBackgroundColorSelectedArray));
        }
        mBackgroundNormal = setColors(mBackgroundNormal, ColorArrayToIntArray(mBackgroundColorNormalArray));

        setBorder();
        setRadiusUI();
        setGradient();
        setBackgroundState();
        return this;
    }

    public RBaseHelper setBackgroundDrawableNormal(Element drawableNormal) {
        this.mBackgroundNormalBmp = drawableNormal;
        if (!isHasPressedBgBmp) {
            mBackgroundPressedBmp = isHasCheckedBgBmp ? mBackgroundCheckedBmp : mBackgroundNormalBmp;
        }
        if (!isHasUnableBgBmp) {
            mBackgroundUnableBmp = mBackgroundNormalBmp;
        }
        if (!isHasCheckedBgBmp) {
            mBackgroundCheckedBmp = mBackgroundNormalBmp;
        }
        if (!isHasSelectedBgBmp) {
            mBackgroundSelectedBmp = mBackgroundNormalBmp;
        }
        refreshStateListDrawable();
        setBackgroundState();
        return this;
    }

    public RBaseHelper setBackgroundColorPressed(Color colorPressed) {
        this.mBackgroundColorPressed = colorPressed;
        this.isHasPressedBgColor = true;
        mBackgroundPressed.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorPressed.getValue()));
        setBackgroundState();
        return this;
    }

    public RBaseHelper setBackgroundColorPressedArray(Color[] colorPressedArray) {
        this.mBackgroundColorPressedArray = colorPressedArray;
        this.isHasPressedBgColor = true;
        mBackgroundPressed = setColors(mBackgroundPressed, ColorArrayToIntArray(mBackgroundColorPressedArray));
        setBorder();
        setRadiusUI();
        setGradient();
        setBackgroundState();
        return this;
    }

    public RBaseHelper setBackgroundDrawablePressed(Element drawablePressed) {
        this.mBackgroundPressedBmp = drawablePressed;
        this.isHasPressedBgBmp = true;
        refreshStateListDrawable();
        setBackgroundState();
        return this;
    }

    public RBaseHelper setBackgroundColorUnable(Color colorUnable) {
        this.mBackgroundColorUnable = colorUnable;
        this.isHasUnableBgColor = true;
        mBackgroundUnable.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorUnable.getValue()));
        setBackgroundState();
        return this;
    }

    public RBaseHelper setBackgroundDrawableUnable(Element drawableUnable) {
        this.mBackgroundUnableBmp = drawableUnable;
        this.isHasUnableBgBmp = true;
        refreshStateListDrawable();
        setBackgroundState();
        return this;
    }

    public RBaseHelper setBackgroundColorUnableArray(Color[] colorUnableArray) {
        this.mBackgroundColorUnableArray = colorUnableArray;
        this.isHasUnableBgColor = true;
        mBackgroundUnable = setColors(mBackgroundUnable, ColorArrayToIntArray(mBackgroundColorUnableArray));
        setBorder();
        setRadiusUI();
        setGradient();
        setBackgroundState();
        return this;
    }

    public RBaseHelper setBackgroundColorChecked(Color colorChecked) {
        this.mBackgroundColorChecked = colorChecked;
        this.isHasCheckedBgColor = true;
        mBackgroundChecked.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorChecked.getValue()));
        if (!isHasPressedBgColor) {
            mBackgroundColorPressed = isHasCheckedBgColor ? mBackgroundColorChecked : mBackgroundColorNormal;
            mBackgroundPressed.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorPressed.getValue()));
        }
        setBackgroundState();
        return this;
    }

    public RBaseHelper setBackgroundColorSelected(Color colorSelected) {
        this.mBackgroundColorSelected = colorSelected;
        this.isHasSelectedBgColor = true;
        mBackgroundSelected.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorSelected.getValue()));
        setBackgroundState();
        return this;
    }

    public RBaseHelper setBackgroundColorCheckedArray(Color[] colorCheckedArray) {
        this.mBackgroundColorCheckedArray = colorCheckedArray;
        this.isHasCheckedBgColor = true;
        mBackgroundChecked = setColors(mBackgroundChecked, ColorArrayToIntArray(mBackgroundColorCheckedArray));
        if (!isHasPressedBgColor) {
            mBackgroundColorPressedArray = isHasCheckedBgColor ? mBackgroundColorCheckedArray : mBackgroundColorNormalArray;
            mBackgroundPressed = setColors(mBackgroundPressed, ColorArrayToIntArray(mBackgroundColorPressedArray));
        }
        setBorder();
        setRadiusUI();
        setGradient();
        setBackgroundState();
        return this;
    }

    public RBaseHelper setBackgroundColorSelectedArray(Color[] colorSelectedArray) {
        this.mBackgroundColorSelectedArray = colorSelectedArray;
        this.isHasSelectedBgColor = true;
        mBackgroundSelected = setColors(mBackgroundSelected, ColorArrayToIntArray(mBackgroundColorSelectedArray));
        setBorder();
        setRadiusUI();
        setGradient();
        setBackgroundState();
        return this;
    }

    public RBaseHelper setBackgroundDrawableChecked(Element drawableChecked) {
        this.mBackgroundCheckedBmp = drawableChecked;
        this.isHasCheckedBgBmp = true;
        if (!isHasPressedBgBmp) mBackgroundPressedBmp = mBackgroundCheckedBmp;
        refreshStateListDrawable();
        setBackgroundState();
        return this;
    }

    public RBaseHelper setBackgroundDrawableSelected(Element drawableSelected) {
        this.mBackgroundSelectedBmp = drawableSelected;
        this.isHasSelectedBgBmp = true;
        refreshStateListDrawable();
        setBackgroundState();
        return this;
    }

    /**
     * 刷新StateListDrawable状态
     * 更新drawable背景时时候刷新
     */
    private void refreshStateListDrawable() {
        mStateBackground = emptyStateListDrawable;
        //unable,focused,pressed,checked,selected,normal
        mStateBackground.addState(states[0], mBackgroundUnableBmp == null ? mBackgroundUnable : mBackgroundUnableBmp);
        mStateBackground.addState(states[1], mBackgroundPressedBmp == null ? mBackgroundPressed : mBackgroundPressedBmp);
        mStateBackground.addState(states[2], mBackgroundPressedBmp == null ? mBackgroundPressed : mBackgroundPressedBmp);
        mStateBackground.addState(states[3], mBackgroundCheckedBmp == null ? mBackgroundChecked : mBackgroundCheckedBmp);
        mStateBackground.addState(states[4], mBackgroundSelectedBmp == null ? mBackgroundSelected : mBackgroundSelectedBmp);
        mStateBackground.addState(states[5], mBackgroundNormalBmp == null ? mBackgroundNormal : mBackgroundNormalBmp);
    }

    private void setBackgroundState() {
        boolean isHasCustom = false;//是否存在自定义
        boolean isHasCusBg, hasCusBorder = false, hasCusCorner = false;//存在自定义相关属性
        boolean isBgColor = mBackgroundColorNormal == null && mBackgroundColorUnable == null && mBackgroundColorPressed == null && mBackgroundColorChecked == null && mBackgroundColorSelected == null;
        boolean isBgColorArray = mBackgroundColorNormalArray == null && mBackgroundColorUnableArray == null && mBackgroundColorPressedArray == null && mBackgroundColorCheckedArray == null && mBackgroundColorSelectedArray == null;
        boolean isBgDrawable = mBackgroundNormalBmp == null && mBackgroundPressedBmp == null && mBackgroundUnableBmp == null && mBackgroundCheckedBmp == null && mBackgroundSelectedBmp == null;

        //是否自定义了背景
        if (isBgColor && isBgColorArray && isBgDrawable) {//未设置自定义背景
            isHasCusBg = false;
        } else {
            isHasCusBg = true;
        }
        //是否自定义了边框
        if (mBorderDashWidth != 0 || mBorderDashGap != 0
                || mBorderWidthNormal != 0 || mBorderWidthPressed != 0 || mBorderWidthUnable != 0 || mBorderWidthChecked != 0 || mBorderWidthSelected != 0
                || mBorderColorNormal != Color.TRANSPARENT || mBorderColorPressed != Color.TRANSPARENT || mBorderColorUnable != Color.TRANSPARENT || mBorderColorChecked != Color.TRANSPARENT || mBorderColorSelected != Color.TRANSPARENT) {
            hasCusBorder = true;
        }

        //是否自定义了圆角
        if (mCornerRadius != -1 || mCornerRadiusTopLeft != 0 || mCornerRadiusTopRight != 0 || mCornerRadiusBottomLeft != 0 || mCornerRadiusBottomRight != 0) {
            hasCusCorner = true;
        }

        if (isHasCusBg || hasCusCorner || hasCusBorder) {
            isHasCustom = true;
        }

        Element mBackgroundDrawable;
        if (!isHasCustom && !useRipple()) {
            mBackgroundDrawable = mViewBackground;//使用原生背景
        } else {
            //获取drawable
            mBackgroundDrawable = getBackgroundDrawable(isHasCustom, mRippleColor);
        }
        mView.setBackground(mBackgroundDrawable);
    }

    /**
     * 获取 BackgroundDrawable
     *
     * @param isCustom 是否存在自定义背景
     * @param rippleColor 水波纹颜色
     */
    private Element getBackgroundDrawable(boolean isCustom, Color rippleColor) {
        if (!isUseRipple()) {
            return mStateBackground;
        } else {
            StateElement stateBackground = new StateElement();
            int[][] states = new int[6][];
            //unable,checked,selected,normal
            states[0] = new int[]{COMPONENT_STATE_DISABLED};//unable
            states[1] = new int[]{COMPONENT_STATE_CHECKED};//checked
            states[2] = new int[]{COMPONENT_STATE_SELECTED};//selected
            states[3] = new int[]{COMPONENT_STATE_FOCUSED};//FOUSED
            states[4] = new int[]{COMPONENT_STATE_PRESSED};//PRESS
            states[5] = new int[]{COMPONENT_STATE_EMPTY};//PRESS
            stateBackground.addState(states[0], mBackgroundUnable);
            stateBackground.addState(states[1], mBackgroundChecked);
            stateBackground.addState(states[2], mBackgroundSelected);
            stateBackground.addState(states[3], mBackgroundPressed);
            stateBackground.addState(states[4], mBackgroundPressed);

            stateBackground.addState(states[5], mBackgroundNormal);
            return stateBackground;
        }
    }

    public boolean useShadow() {
        return mShadowRadius >= 0;
    }

    public RBaseHelper setShadowRadius(int shadowRadius) {
        this.mShadowRadius = shadowRadius;
        setBackgroundState();
        return this;
    }

    public int getShadowRadius() {
        return mShadowRadius;
    }

    public RBaseHelper setShadowColor(Color shadowColor) {
        this.mShadowColor = shadowColor;
        setBackgroundState();
        return this;
    }

    public Color getShadowColor() {
        return mShadowColor;
    }

    public RBaseHelper setShadowDx(int shadowDx) {
        this.mShadowDx = shadowDx;
        setBackgroundState();
        return this;
    }

    public int getShadowDx() {
        return mShadowDx;
    }

    public RBaseHelper setShadowDy(int shadowDy) {
        this.mShadowDy = shadowDy;
        setBackgroundState();
        return this;
    }

    public int getShadowDy() {
        return mShadowDy;
    }

    public RBaseHelper setUseRipple(boolean isUseRipple) {
        this.isUseRipple = isUseRipple;
        setBackgroundState();
        return this;
    }

    public boolean useRipple() {
        return isUseRipple;
    }

    public RBaseHelper setRippleColor(Color rippleColor) {
        this.mRippleColor = rippleColor;
        this.isUseRipple = true;
        setBackgroundState();
        return this;
    }

    public Color getRippleColor() {
        return mRippleColor;
    }

    public RBaseHelper setRippleMaskDrawable(Element rippleMaskDrawable) {
        this.mRippleMaskDrawable = rippleMaskDrawable;
        this.isUseRipple = true;
        int MASK_STYLE_DRAWABLE = 3;
        this.mRippleMaskStyle = MASK_STYLE_DRAWABLE;
        setBackgroundState();
        return this;
    }

    public Element getRippleMaskDrawable() {
        return mRippleMaskDrawable;
    }

    public RBaseHelper setBorderWidthNormal(int width) {
        this.mBorderWidthNormal = width;
        if (!isHasPressedBorderWidth) {
            mBorderWidthPressed = isHasCheckedBorderWidth ? mBorderWidthChecked : mBorderWidthNormal;
            setBorderPressed();
        }
        if (!isHasUnableBorderWidth) {
            mBorderWidthUnable = mBorderWidthNormal;
            setBorderUnable();
        }
        if (!isHasCheckedBorderWidth) {
            mBorderWidthChecked = mBorderWidthNormal;
            setBorderChecked();
        }
        if (!isHasSelectedBorderWidth) {
            mBorderWidthSelected = mBorderWidthNormal;
            setBorderSelected();
        }
        setBorderNormal();
        return this;
    }

    public int getBorderWidthNormal() {
        return mBorderWidthNormal;
    }

    public RBaseHelper setBorderColorNormal(Color color) {
        this.mBorderColorNormal = color;
        if (!isHasPressedBorderColor) {
            new ToastDialog(mContext).setText("999").show();
            mBorderColorPressed = isHasCheckedBorderColor ? mBorderColorChecked : mBorderColorNormal;
            setBorderPressed();
        }
        if (!isHasUnableBorderColor) {
            mBorderColorUnable = mBorderColorNormal;
            setBorderUnable();
        }
        if (!isHasCheckedBorderColor) {
            mBorderColorChecked = mBorderColorNormal;
            setBorderChecked();
        }
        if (!isHasSelectedBorderColor) {
            mBorderColorSelected = mBorderColorNormal;
            setBorderSelected();
        }
        setBorderNormal();
        return this;
    }

    public Color getBorderColorNormal() {
        return mBorderColorNormal;
    }

    public RBaseHelper setBorderWidthPressed(int width) {
        this.mBorderWidthPressed = width;
        this.isHasPressedBorderWidth = true;
        setBorderPressed();
        return this;
    }

    public int getBorderWidthPressed() {
        return mBorderWidthPressed;
    }

    public RBaseHelper setBorderColorPressed(Color color) {
        this.mBorderColorPressed = color;
        this.isHasPressedBorderColor = true;
        setBorderPressed();
        return this;
    }

    public Color getBorderColorPressed() {
        return mBorderColorPressed;
    }

    public RBaseHelper setBorderColorChecked(Color color) {
        this.mBorderColorChecked = color;
        this.isHasCheckedBorderColor = true;
        if (!isHasPressedBorderColor) mBorderColorPressed = mBorderColorChecked;
        setBorderChecked();
        return this;
    }

    public Color getBorderColorChecked() {
        return mBorderColorChecked;
    }

    public RBaseHelper setBorderColorSelected(Color color) {
        this.mBorderColorSelected = color;
        this.isHasSelectedBorderColor = true;
        setBorderSelected();
        return this;
    }

    public Color getBorderColorSelected() {
        return mBorderColorSelected;
    }

    public RBaseHelper setBorderWidthChecked(int width) {
        this.mBorderWidthChecked = width;
        this.isHasCheckedBorderWidth = true;
        if (!isHasPressedBorderWidth) mBorderWidthPressed = mBorderWidthChecked;
        setBorderChecked();
        return this;
    }

    public int getBorderWidthChecked() {
        return mBorderWidthChecked;
    }

    public RBaseHelper setBorderWidthSelected(int width) {
        this.mBorderWidthSelected = width;
        this.isHasSelectedBorderWidth = true;
        setBorderChecked();
        return this;
    }

    public int getBorderWidthSelected() {
        return mBorderWidthSelected;
    }

    public RBaseHelper setBorderWidthUnable(int width) {
        this.mBorderWidthUnable = width;
        this.isHasUnableBorderWidth = true;
        setBorderUnable();
        return this;
    }

    public int getBorderWidthUnable() {
        return mBorderWidthUnable;
    }

    public RBaseHelper setBorderColorUnable(Color color) {
        this.mBorderColorUnable = color;
        this.isHasUnableBorderColor = true;
        setBorderUnable();
        return this;
    }

    public Color getBorderColorUnable() {
        return mBorderColorUnable;
    }

    public RBaseHelper setBorderWidth(int normal, int pressed, int unable, int checked,
                                      int selected) {
        this.mBorderWidthNormal = normal;
        this.mBorderWidthPressed = pressed;
        this.mBorderWidthUnable = unable;
        this.mBorderWidthChecked = checked;
        this.mBorderWidthSelected = selected;
        this.isHasPressedBorderWidth = true;
        this.isHasUnableBorderWidth = true;
        this.isHasCheckedBorderWidth = true;
        this.isHasSelectedBorderWidth = true;
        setBorder();
        return this;
    }

    public RBaseHelper setBorderColor(Color normal, Color pressed,
                                      Color unable, Color checked, Color selected) {
        this.mBorderColorNormal = normal;
        this.mBorderColorPressed = pressed;
        this.mBorderColorUnable = unable;
        this.mBorderColorChecked = checked;
        this.mBorderColorSelected = selected;
        this.isHasPressedBorderColor = true;
        this.isHasUnableBorderColor = true;
        this.isHasCheckedBorderColor = true;
        this.isHasSelectedBorderColor = true;
        setBorder();
        return this;
    }

    public RBaseHelper setBorderDashWidth(float dashWidth) {
        this.mBorderDashWidth = dashWidth;
        setBorder();
        return this;
    }

    public float getBorderDashWidth() {
        return mBorderDashWidth;
    }

    public RBaseHelper setBorderDashGap(float dashGap) {
        this.mBorderDashGap = dashGap;
        setBorder();
        return this;
    }

    public float getBorderDashGap() {
        return mBorderDashGap;
    }

    public RBaseHelper setBorderDash(float dashWidth, float dashGap) {
        this.mBorderDashWidth = dashWidth;
        this.mBorderDashGap = dashGap;
        setBorder();
        return this;
    }

    private void setBorder() {
        float[] floats = new float[2];
        floats[0] = mBorderDashGap;
        floats[1] = mBorderDashWidth;

        mBackgroundNormal.setStroke(mBorderWidthNormal, RgbColor.fromArgbInt(mBorderColorNormal.getValue()));
        mBackgroundNormal.setDashPathEffectValues(floats, 0);
        mBackgroundPressed.setStroke(mBorderWidthPressed, RgbColor.fromArgbInt(mBorderColorPressed.getValue()));
        mBackgroundPressed.setDashPathEffectValues(floats, 0);
        mBackgroundUnable.setStroke(mBorderWidthUnable, RgbColor.fromArgbInt(mBorderColorUnable.getValue()));
        mBackgroundUnable.setDashPathEffectValues(floats, 0);
        mBackgroundChecked.setStroke(mBorderWidthChecked, RgbColor.fromArgbInt(mBorderColorChecked.getValue()));
        mBackgroundChecked.setDashPathEffectValues(floats, 0);
        mBackgroundSelected.setStroke(mBorderWidthSelected, RgbColor.fromArgbInt(mBorderColorSelected.getValue()));
        mBackgroundSelected.setDashPathEffectValues(floats, 0);

        setBackgroundState();
    }

    private void setBorderNormal() {
        mBackgroundNormal.setStroke(mBorderWidthNormal, RgbColor.fromArgbInt(mBorderColorNormal.getValue()));
        setBackgroundState();
    }

    private void setBorderPressed() {
        mBackgroundPressed.setStroke(mBorderWidthPressed, RgbColor.fromArgbInt(mBorderColorPressed.getValue()));
        setBackgroundState();
    }

    private void setBorderUnable() {
        mBackgroundUnable.setStroke(mBorderWidthUnable, RgbColor.fromArgbInt(mBorderColorUnable.getValue()));
        setBackgroundState();
    }

    private void setBorderChecked() {
        mBackgroundChecked.setStroke(mBorderWidthChecked, RgbColor.fromArgbInt(mBorderColorChecked.getValue()));
        setBackgroundState();
    }

    private void setBorderSelected() {
        mBackgroundSelected.setStroke(mBorderWidthSelected, RgbColor.fromArgbInt(mBorderColorSelected.getValue()));
        setBackgroundState();
    }

    /*********************
     * radius
     ********************/

    public void setCornerRadius(float radius) {
        this.mCornerRadius = radius;
        setRadiusValue();
    }

    public float getCornerRadius() {
        return mCornerRadius;
    }

    public RBaseHelper setCornerRadiusTopLeft(float topLeft) {
        this.mCornerRadius = -1;
        this.mCornerRadiusTopLeft = topLeft;
        setRadiusValue();
        return this;
    }

    public float getCornerRadiusTopLeft() {
        return mCornerRadiusTopLeft;
    }

    public RBaseHelper setCornerRadiusTopRight(float topRight) {
        this.mCornerRadius = -1;
        this.mCornerRadiusTopRight = topRight;
        setRadiusValue();
        return this;
    }

    public float getCornerRadiusTopRight() {
        return mCornerRadiusTopRight;
    }

    public RBaseHelper setCornerRadiusBottomRight(float bottomRight) {
        this.mCornerRadius = -1;
        this.mCornerRadiusBottomRight = bottomRight;
        setRadiusValue();
        return this;
    }

    public float getCornerRadiusBottomRight() {
        return mCornerRadiusBottomRight;
    }

    public RBaseHelper setCornerRadiusBottomLeft(float bottomLeft) {
        this.mCornerRadius = -1;
        this.mCornerRadiusBottomLeft = bottomLeft;
        setRadiusValue();
        return this;
    }

    public float getCornerRadiusBottomLeft() {
        return mCornerRadiusBottomLeft;
    }

    public RBaseHelper setCornerRadius(float topLeft, float topRight, float bottomRight,
                                       float bottomLeft) {
        this.mCornerRadius = -1;
        this.mCornerRadiusTopLeft = topLeft;
        this.mCornerRadiusTopRight = topRight;
        this.mCornerRadiusBottomRight = bottomRight;
        this.mCornerRadiusBottomLeft = bottomLeft;
        setRadiusValue();
        return this;
    }

    /**
     * 设置圆角UI
     */
    private void setRadiusUI() {
        mBackgroundNormal.setCornerRadiiArray(mBorderRadii);
        mBackgroundPressed.setCornerRadiiArray(mBorderRadii);
        mBackgroundUnable.setCornerRadiiArray(mBorderRadii);
        mBackgroundChecked.setCornerRadiiArray(mBorderRadii);
        mBackgroundSelected.setCornerRadiiArray(mBorderRadii);
        setBackgroundState();
    }

    /**
     * 设置圆角数值
     */
    private void setRadiusValue() {
        if (mCornerRadius >= 0) {
            mBorderRadii[0] = DensityUtils.pxToFp(mContext, mCornerRadius);
            mBorderRadii[1] = DensityUtils.pxToFp(mContext, mCornerRadius);
            mBorderRadii[2] = DensityUtils.pxToFp(mContext, mCornerRadius);
            mBorderRadii[3] = DensityUtils.pxToFp(mContext, mCornerRadius);
            mBorderRadii[4] = DensityUtils.pxToFp(mContext, mCornerRadius);
            mBorderRadii[5] = DensityUtils.pxToFp(mContext, mCornerRadius);
            mBorderRadii[6] = DensityUtils.pxToFp(mContext, mCornerRadius);
            mBorderRadii[7] = DensityUtils.pxToFp(mContext, mCornerRadius);
            setRadiusUI();
            return;
        }

        if (mCornerRadius < 0) {
            mBorderRadii[0] = DensityUtils.pxToFp(mContext, mCornerRadiusTopLeft);
            mBorderRadii[1] = DensityUtils.fpToPx(mContext, mCornerRadiusTopLeft);
            mBorderRadii[2] = DensityUtils.fpToPx(mContext, mCornerRadiusTopRight);
            mBorderRadii[3] = DensityUtils.fpToPx(mContext, mCornerRadiusTopRight);
            mBorderRadii[4] = DensityUtils.fpToPx(mContext, mCornerRadiusBottomRight);
            mBorderRadii[5] = DensityUtils.fpToPx(mContext, mCornerRadiusBottomRight);
            mBorderRadii[6] = DensityUtils.fpToPx(mContext, mCornerRadiusBottomLeft);
            mBorderRadii[7] = DensityUtils.fpToPx(mContext, mCornerRadiusBottomLeft);
            setRadiusUI();
            return;
        }
    }

    /**
     * 设置View大小变化监听,用来更新渐变半径
     */
    private void addOnGlobalLayoutListener() {
    }

    /**
     * 获取背景颜色
     * 备注:
     * 数组[0]:背景类型(1:单一颜色 2:颜色数组 3:图片资源)
     * 数组[1]:单一颜色,可能为0
     * 数组[2]:颜色数组,可能为null
     * 数组[3]:图片资源drawable,可能为null
     * 单个图片背景 或多个颜色背景
     *
     * @return 数组
     */
    private Object[] getBackgroundInfo(AttrSet attr, String key) {
        Color[] colors = null;

        int bgType = BG_TYPE_COLOR;//背景类型
        String[] bgColorArray = null;//多个颜色
        Color bgColor = null; // 单一颜色
        Element bgElement = null;
        if (strBgType.equals("color")) {
            bgType = BG_TYPE_COLOR;
            bgColor = getStringColor(attr, key, "#ffffff");
        } else if (strBgType.equals("colorArray")) {
            String colorList = AttrUtils.getXmlManyValue(attr, key, null);

            if (colorList != null) {
                bgColorArray = colorList.split(",");
                colors = new Color[bgColorArray.length];
                for (int i = 0; i < bgColorArray.length; i++) {
                    colors[i] = new Color(Color.getIntColor(bgColorArray[i]));
                }
                bgType = BG_TYPE_COLOR_ARRAY;
            }
        } else if (strBgType.equals("element")) {
            bgType = BG_TYPE_IMG;
            // 图片背景类型
            bgElement = AttrUtils.getElement(attr, key, null);
        }
        return new Object[]{bgType, bgColor, colors, bgElement};
    }

    /**
     * 获取渐变方向
     *
     * @return ShapeElement.Orientation
     */
    private ShapeElement.Orientation getGradientOrientation(AttrSet attr) {
        String orientationValue = AttrUtils.getString(attr, gradient_orientation, BL_TR).toUpperCase();
        ShapeElement.Orientation orientation = ShapeElement.Orientation.BOTTOM_LEFT_TO_TOP_RIGHT;
        switch (orientationValue) {
            case TOP_BOTTOM:
                orientation = ShapeElement.Orientation.TOP_TO_BOTTOM;
                break;
            case TR_BL:
                orientation = ShapeElement.Orientation.TOP_RIGHT_TO_BOTTOM_LEFT;
                break;
            case RIGHT_LEFT:
                orientation = ShapeElement.Orientation.RIGHT_TO_LEFT;
                break;
            case BR_TL:
                orientation = ShapeElement.Orientation.BOTTOM_RIGHT_TO_TOP_LEFT;
                break;
            case BOTTOM_TOP:
                orientation = ShapeElement.Orientation.BOTTOM_TO_TOP;
                break;
            case BL_TR:
                orientation = ShapeElement.Orientation.BOTTOM_LEFT_TO_TOP_RIGHT;
                break;
            case LEFT_RIGHT:
                orientation = ShapeElement.Orientation.LEFT_TO_RIGHT;
                break;
            case TL_BR:
                orientation = ShapeElement.Orientation.TOP_LEFT_TO_BOTTOM_RIGHT;
                break;
        }
        return orientation;
    }

    /**
     * 设置GradientDrawable颜色数组,兼容版本
     *
     * @param drawable GradientDrawable
     * @param colors 颜色数组
     * @return ShapeElement
     */
    private ShapeElement setColors(ShapeElement drawable, int[] colors) {
        if (drawable == null) drawable = new ShapeElement();
        drawable = new ShapeElement();
        drawable.setOrientation(mGradientOrientation);
        RgbColor[] rgbColors = new RgbColor[colors.length];
        for (int i = 0; i < colors.length; i++) {
            RgbColor color = RgbColor.fromArgbInt(colors[i]);
            rgbColors[i] = color;
        }
        drawable.setRgbColors(rgbColors);
        return drawable;
    }

    /**
     * 是否移出view
     *
     * @param x x距离
     * @param y y距离
     * @return boolean
     */
    protected boolean isOutsideView(int x, int y) {
        boolean isFlag = false;
        // Be lenient about moving outside of buttons
        int mTouchSlop = 0;
        if ((x < 0 - mTouchSlop) || (x >= mView.getWidth() + mTouchSlop) ||
                (y < 0 - mTouchSlop) || (y >= mView.getHeight() + mTouchSlop)) {
            // Outside button
            isFlag = true;
        }
        return isFlag;
    }

    /**
     * 是否使用Ripple
     *
     * @return boolean
     */
    private boolean isUseRipple() {
        return isUseRipple;
    }

    /**
     * 初始化Clip
     */
    private void initClip() {
        //初始化clip
        mClipHelper.initClip(mView, isClipLayout, new ClipPathManager.ClipPathCreator() {
            @Override
            public Path createClipPath(int width, int height) {
                Path path = new Path();
                path.addRoundRect(new RectFloat(0, 0, width, height), mBorderRadii, Path.Direction.COUNTER_CLOCK_WISE);
                return path;
            }
        });
    }

    @Override
    public void dispatchDraw(Canvas canvas) {
        mClipHelper.dispatchDraw(canvas);
    }

    @Override
    public void onLayout(int left, int top, int right, int bottom) {
        mClipHelper.onLayout(left, top, right, bottom);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        if (isUseRipple && mView.isEnabled()) {
            // 水波纹相关属性
            MmiPoint pointerPosition = event.getPointerPosition(0);
            float x = pointerPosition.getX();
            float y = pointerPosition.getY();
            int[] parentLocationOnScreen = mView.getLocationOnScreen();
            int yy = (int) (y - parentLocationOnScreen[1]);
            sx = x - parentLocationOnScreen[0];
            sy = yy + 300;
            animatorValue.start();
        }
        return false;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (isUseRipple && mView.isEnabled()) {
            animatorValue.setValueUpdateListener((animatorValue, v) -> {
                zj = v;
                if (v > 0.85) {
                    paint.setAlpha(0f);
                } else {
                    paint.setAlpha(0.2f);
                }
                mView.invalidate();
            });
            if (mView.getWidth() > mView.getHeight()) {
                canvas.drawCircle(sx, sy, mView.getWidth() * zj, paint);
            } else {
                canvas.drawCircle(sx, sy, mView.getHeight() * zj, paint);
            }
        }
    }

    @Override
    public void onRefreshed(Component component) {
    }
}
