package com.org.matomo.demo.chechbox;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.ResourceManager;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

/**
 * TextView-Helper
 *
 * @author ZhongDaFeng
 */
public class RTextViewHelper extends RBaseHelper<Text> implements ITextViewFeature, Text.TextObserver,
        Component.TouchEventListener, Component.DrawTask {
    public static final int ICON_DIR_LEFT = 1, ICON_DIR_TOP = 2, ICON_DIR_RIGHT = 3, ICON_DIR_BOTTOM = 4;

    /**
     * 是否设置对应的属性
     */
    protected boolean isHasPressedTextColor = false;
    protected boolean isHasUnableTextColor = false;
    protected boolean isHasSelectedTextColor = false;

    // Text
    protected Color mTextColorNormal;
    protected Color mTextColorPressed;
    protected Color mTextColorUnable;
    protected Color mTextColorSelected;
    protected int[][] states = new int[6][];

    // Icon（兼容老版本）

    private Element mIcon = null;
    private Element mIconNormal;
    private Element mIconPressed;
    private Element mIconUnable;
    private Element mIconSelected;
    private int mIconHeight;
    private int mIconWidth;
    private int mIconDirection;

    // icon
    private int mIconHeightLeft;
    private int mIconWidthLeft;
    private int mIconHeightRight;
    private int mIconWidthRight;

    private int mIconHeightTop;
    private int mIconWidthTop;
    private int mIconHeightBottom;
    private int mIconWidthBottom;

    private Element mIconLeft = null;
    private Element mIconNormalLeft;
    private Element mIconPressedLeft;
    private Element mIconUnableLeft;
    private Element mIconSelectedLeft;

    private Element mIconTop = null;
    private Element mIconNormalTop;
    private Element mIconPressedTop;
    private Element mIconUnableTop;
    private Element mIconSelectedTop;

    private Element mIconBottom = null;
    private Element mIconNormalBottom;
    private Element mIconPressedBottom;
    private Element mIconUnableBottom;
    private Element mIconSelectedBottom;

    private Element mIconRight = null;
    private Element mIconNormalRight;
    private Element mIconPressedRight;
    private Element mIconUnableRight;
    private Element mIconSelectedRight;

    // typeface

    private String mTypefacePath;

    private boolean isDrawableWithText = false;

    // TextView本身设置的padding
    protected int mPaddingLeft, mPaddingRight, mPaddingTop, mPaddingBottom;
    private EventHandler eventHandler;

    // 水波纹相关属性
    private MmiPoint pointerPosition;
    private int sy;
    private float sx;
    AnimatorValue animatorValue = new AnimatorValue();
    private float zj;
    private Paint paint;
    private Color text_ripple_color;
    private boolean isRipple;

    public RTextViewHelper(Context context, Text view, AttrSet attrs) {
        super(context, view, attrs);
        mView.setTouchEventListener(this::onTouchEvent);
        mView.addDrawTask(this);
        mView.setTouchEventListener(this);

        eventHandler = new EventHandler(EventRunner.getMainEventRunner());
        initAttributeSet(context, attrs);
        paint = new Paint();
        paint.setColor(text_ripple_color);

        paint.setAlpha(0.2f);
        paint.setStyle(Paint.Style.FILL_STYLE);
        animatorValue.setDuration(500);
        animatorValue.setCurveType(Animator.CurveType.LINEAR);

        // 监听View改变

        addOnViewChangeListener();
    }

    /**
     * 设置变化监听
     */
    private void addOnViewChangeListener() {
        if (mView == null) {
            return;
        }
        if (!isDrawableWithText) {
            return;
        }

        // 文本改变
        mView.addTextObserver(this::onTextUpdated);
    }

    @Override
    public void onTextUpdated(String s, int i, int i1, int i2) {
        setIcon();
    }

    /**
     * 初始化控件属性
     *
     * @param context
     * @param attrs
     */
    private void initAttributeSet(Context context, AttrSet attrs) {
        if (context == null || attrs == null) {
            setup();
            return;
        }

        Element a = TypedAttrUtils.getDrawable(attrs, "RTextView", null);

        // icon
        text_ripple_color = AttrUtils.getStringToColor(attrs, "text_ripple_color", Color.TRANSPARENT);
        isRipple = AttrUtils.getStringToBool(attrs, "text_ripple", false);
        mIconNormalLeft = TypedAttrUtils.getDrawable(attrs, "icon_normal_left", null);
        mIconPressedLeft = TypedAttrUtils.getDrawable(attrs, "icon_pressed_left", null);
        mIconUnableLeft = TypedAttrUtils.getDrawable(attrs, "icon_unable_left", null);
        mIconSelectedLeft = TypedAttrUtils.getDrawable(attrs, "icon_selected_left", null);
        mIconNormalRight = TypedAttrUtils.getDrawable(attrs, "icon_normal_right", null);
        mIconPressedRight = TypedAttrUtils.getDrawable(attrs, "icon_pressed_right", null);
        mIconUnableRight = TypedAttrUtils.getDrawable(attrs, "icon_unable_right", null);
        mIconSelectedRight = TypedAttrUtils.getDrawable(attrs, "icon_selected_right", null);
        mIconNormalTop = TypedAttrUtils.getDrawable(attrs, "icon_normal_top", null);
        mIconPressedTop = TypedAttrUtils.getDrawable(attrs, "icon_pressed_top", null);
        mIconUnableTop = TypedAttrUtils.getDrawable(attrs, "icon_unable_top", null);
        mIconSelectedTop = TypedAttrUtils.getDrawable(attrs, "icon_selected_top", null);
        mIconNormalBottom = TypedAttrUtils.getDrawable(attrs, "icon_normal_bottom", null);
        mIconPressedBottom = TypedAttrUtils.getDrawable(attrs, "icon_pressed_bottom", null);
        mIconUnableBottom = TypedAttrUtils.getDrawable(attrs, "icon_unable_bottom", null);
        mIconSelectedBottom = TypedAttrUtils.getDrawable(attrs, "icon_selected_bottom", null);

        // 版本兼容代码
        mIconNormal = TypedAttrUtils.getDrawable(attrs, "icon_src_normal", null);
        mIconPressed = TypedAttrUtils.getDrawable(attrs, "icon_src_pressed", null);
        mIconUnable = TypedAttrUtils.getDrawable(attrs, "icon_src_unable", null);
        mIconSelected = TypedAttrUtils.getDrawable(attrs, "icon_src_selected", null);

        mIconWidthLeft = TypedAttrUtils.getDimensionPixelSize(attrs, "icon_width_left", 0);

        mIconHeightLeft = TypedAttrUtils.getDimensionPixelSize(attrs, "icon_height_left", 0);
        mIconWidthRight = TypedAttrUtils.getDimensionPixelSize(attrs, "icon_width_right", 0);
        mIconHeightRight = TypedAttrUtils.getDimensionPixelSize(attrs, "icon_height_right", 0);
        mIconWidthBottom = TypedAttrUtils.getDimensionPixelSize(attrs, "icon_width_bottom", 0);
        mIconHeightBottom = TypedAttrUtils.getDimensionPixelSize(attrs, "icon_height_bottom", 0);
        mIconWidthTop = TypedAttrUtils.getDimensionPixelSize(attrs, "icon_width_top", 0);
        mIconHeightTop = TypedAttrUtils.getDimensionPixelSize(attrs, "icon_height_top", 0);

        // 兼容代码
        mIconWidth = TypedAttrUtils.getDimensionPixelSize(attrs, "icon_width", 0);
        mIconHeight = TypedAttrUtils.getDimensionPixelSize(attrs, "icon_height", 0);
        mIconDirection = AttrUtils.getInt(attrs, "icon_direction", ICON_DIR_LEFT);
        mTextColorNormal = AttrUtils.getColor(attrs, "text_color_normal", mView.getTextColor());
        mTextColorPressed = AttrUtils.getStringToColor(attrs, "text_color_pressed", mView.getTextColor());
        mTextColorUnable = AttrUtils.getStringToColor(attrs, "text_color_unable", mView.getTextColor());
        mTextColorSelected = AttrUtils.getStringToColor(attrs, "text_color_selected", mView.getTextColor());

        // typeface
        mTypefacePath = TypedAttrUtils.getString(attrs, "text_typeface", null);

        // drawableWithText
        isDrawableWithText = AttrUtils.getStringToBool(attrs, "icon_with_text", false);
        isHasPressedTextColor = mTextColorPressed != null;
        isHasUnableTextColor = mTextColorUnable != null;
        isHasSelectedTextColor = mTextColorSelected != null;

        // setup
        setup();
    }

    /**
     * 设置
     */
    private void setup() {
        /**
         * icon
         */
        if (!mView.isEnabled()) {
            mIcon = mIconUnable;
            mIconLeft = mIconUnableLeft;
            mIconRight = mIconUnableRight;
            mIconTop = mIconUnableTop;
            mIconBottom = mIconUnableBottom;
        } else if (mView.isSelected()) {
            mIcon = mIconSelected;
            mIconLeft = mIconSelectedLeft;
            mIconRight = mIconSelectedRight;
            mIconTop = mIconSelectedTop;
            mIconBottom = mIconSelectedBottom;
        } else {
            mIcon = mIconNormal;
            mIconLeft = mIconNormalLeft;
            mIconRight = mIconNormalRight;
            mIconTop = mIconNormalTop;
            mIconBottom = mIconNormalBottom;
        }

        /**
         * 设置文字颜色默认值
         */
        if (!isHasPressedTextColor) {
            mTextColorPressed = mTextColorNormal;
        }
        if (!isHasUnableTextColor) {
            mTextColorUnable = mTextColorNormal;
        }
        if (!isHasSelectedTextColor) {
            mTextColorSelected = mTextColorNormal;
        }

        // unable,focused,pressed,selected,normal
        states[0] = new int[]{-ComponentState.COMPONENT_STATE_DISABLED}; // unable

        states[1] = new int[]{ComponentState.COMPONENT_STATE_FOCUSED}; // focused
        states[2] = new int[]{ComponentState.COMPONENT_STATE_PRESSED}; // pressed
        states[3] = new int[]{ComponentState.COMPONENT_STATE_CHECKED}; // checked
        states[4] = new int[]{ComponentState.COMPONENT_STATE_SELECTED}; // selected
        states[5] = new int[]{ComponentState.COMPONENT_STATE_EMPTY}; // normal

        // 设置文本颜色
        setTextColor();

        // 设置ICON
        setIcon();

        // 设置文本字体样式
        setTypeface();
    }

    /**
     * 字型
     *
     * @param typefacePath 路径
     * @return this
     */

    public RTextViewHelper setTypeface(String typefacePath) {
        this.mTypefacePath = typefacePath;
        setTypeface();
        return this;
    }

    public String getTypefacePath() {
        return mTypefacePath;
    }

    private void setTypeface() {
        if (mTypefacePath != null) {
            ResourceManager resourceManager = mContext.getResourceManager();
            RawFileEntry rawFileEntry = resourceManager.getRawFileEntry(mTypefacePath);
            Font builder = new Font.Builder(rawFileEntry.toString()).build();
            mView.setFont(builder);
        }
    }

    /**
     * 默认图片
     *
     * @param element
     * @return 图片
     */
    @Deprecated
    public RTextViewHelper setIconNormal(Element element) {
        this.mIconNormal = element;
        this.mIcon = element;
        setIcon();
        return this;
    }

    /**
     * 获取图片信息
     *
     * @return 图片
     */
    @Deprecated
    public Element getIconNormal() {
        return mIconNormal;
    }

    /**
     * 按下图片
     *
     * @param icon
     * @return 返回图片
     */
    @Deprecated
    public RTextViewHelper setIconPressed(Element icon) {
        this.mIconPressed = icon;
        this.mIcon = icon;
        setIcon();
        return this;
    }

    /**
     * 获取按下后的图片
     *
     * @return 图片信息
     */
    @Deprecated
    public Element getIconPressed() {
        return mIconPressed;
    }

    /**
     * 设置图片
     *
     * @param icon
     * @return 图
     */
    @Deprecated
    public RTextViewHelper setIconUnable(Element icon) {
        this.mIconUnable = icon;
        this.mIcon = icon;
        setIcon();
        return this;
    }

    /**
     * 获取图
     *
     * @return 图信息
     */
    @Deprecated
    public Element getIconUnable() {
        return mIconUnable;
    }

    /**
     * 选中图
     *
     * @param icon
     * @return 图
     */
    @Deprecated
    public RTextViewHelper setIconSelected(Element icon) {
        this.mIconSelected = icon;
        this.mIcon = icon;
        setIcon();
        return this;
    }

    @Deprecated
    public Element getIconSelected() {
        return mIconSelected;
    }

    /**
     * 图的大小
     *
     * @param iconWidth
     * @param iconHeight
     * @return 信息
     */
    @Deprecated
    public RTextViewHelper setIconSize(int iconWidth, int iconHeight) {
        this.mIconWidth = iconWidth;
        this.mIconHeight = iconHeight;
        setIcon();
        return this;
    }

    @Deprecated
    public RTextViewHelper setIconWidth(int iconWidth) {
        this.mIconWidth = iconWidth;
        setIcon();
        return this;
    }

    @Deprecated
    public int getIconWidth() {
        return mIconWidth;
    }

    @Deprecated
    public RTextViewHelper setIconHeight(int iconHeight) {
        this.mIconHeight = iconHeight;
        setIcon();
        return this;
    }

    @Deprecated
    public int getIconHeight() {
        return mIconHeight;
    }

    @Deprecated
    public RTextViewHelper setIconDirection(int iconDirection) {
        this.mIconDirection = iconDirection;
        setIcon();
        return this;
    }

    @Deprecated
    public int getIconDirection() {
        return mIconDirection;
    }

    /**
     * 默认背景色
     *
     * @param icon 背景
     * @return this
     */
    public RTextViewHelper setIconNormalLeft(Element icon) {
        this.mIconNormalLeft = icon;
        this.mIconLeft = icon;
        setIcon();
        return this;
    }

    /**
     * 设置图片右边
     *
     * @param icon
     * @return 默认图右边
     */
    public RTextViewHelper setIconNormalRight(Element icon) {
        this.mIconNormalRight = icon;
        this.mIconRight = icon;
        setIcon();
        return this;
    }

    /**
     * 设置默认图
     *
     * @param icon
     * @return 默认图上面
     */
    public RTextViewHelper setIconNormalTop(Element icon) {
        this.mIconNormalTop = icon;
        this.mIconTop = icon;
        setIcon();
        return this;
    }

    /**
     * 设置默认图下面
     *
     * @param icon
     * @return 图下面
     */
    public RTextViewHelper setIconNormalBottom(Element icon) {
        this.mIconNormalBottom = icon;
        this.mIconBottom = icon;
        setIcon();
        return this;
    }

    public Element getIconNormalLeft() {
        return mIconNormalLeft;
    }

    public Element getIconNormalRight() {
        return mIconNormalRight;
    }

    public Element getIconNormalTop() {
        return mIconNormalTop;
    }

    public Element getIconNormalBottom() {
        return mIconNormalBottom;
    }

    /**
     * 点击后的置于左方图片
     *
     * @param icon
     * @return 图
     */
    public RTextViewHelper setIconPressedLeft(Element icon) {
        this.mIconPressedLeft = icon;
        this.mIconLeft = icon;
        setIcon();
        return this;
    }

    /**
     * 点击的置于右方图片
     *
     * @param icon
     * @return 图
     */
    public RTextViewHelper setIconPressedRight(Element icon) {
        this.mIconPressedRight = icon;
        this.mIconRight = icon;
        setIcon();
        return this;
    }

    /**
     * 点击后的置于上方图片
     *
     * @param icon
     * @return 图
     */
    public RTextViewHelper setIconPressedTop(Element icon) {
        this.mIconPressedTop = icon;
        this.mIconTop = icon;
        setIcon();
        return this;
    }

    /**
     * 点击后的置于下方图片
     *
     * @param icon
     * @return 图片信息
     */
    public RTextViewHelper setIconPressedBottom(Element icon) {
        this.mIconPressedBottom = icon;
        this.mIconBottom = icon;
        setIcon();
        return this;
    }

    public Element getIconPressedLeft() {
        return mIconPressedLeft;
    }

    public Element getIconPressedRight() {
        return mIconPressedRight;
    }

    public Element getIconPressedTop() {
        return mIconPressedTop;
    }

    public Element getIconPressedBottom() {
        return mIconPressedBottom;
    }

    /**
     * 未选中的置于左方图片
     *
     * @param icon 初始左边icon
     * @return this
     */
    public RTextViewHelper setIconUnableLeft(Element icon) {
        this.mIconUnableLeft = icon;
        this.mIconLeft = icon;
        setIcon();
        return this;
    }

    /**
     * 未选中的置于右方图片
     *
     * @param icon 初始右边icon
     * @return this
     */
    public RTextViewHelper setIconUnableRight(Element icon) {
        this.mIconUnableRight = icon;
        this.mIconRight = icon;
        setIcon();
        return this;
    }

    /**
     * 未选中的置于上方图片
     *
     * @param icon
     * @return 图片信息
     */
    public RTextViewHelper setIconUnableTop(Element icon) {
        this.mIconUnableTop = icon;
        this.mIconTop = icon;
        setIcon();
        return this;
    }

    /**
     * 未选中的置于下方图片
     *
     * @param icon
     * @return 图片信息
     */
    public RTextViewHelper setIconUnableBottom(Element icon) {
        this.mIconUnableBottom = icon;
        this.mIconBottom = icon;
        setIcon();
        return this;
    }

    public Element getIconUnableLeft() {
        return mIconUnableLeft;
    }

    public Element getIconUnableRight() {
        return mIconUnableRight;
    }

    public Element getIconUnableTop() {
        return mIconUnableTop;
    }

    public Element getIconUnableBottom() {
        return mIconUnableBottom;
    }

    /**
     * 选中的置于左方图片
     *
     * @param icon
     * @return 图片信息
     */
    public RTextViewHelper setIconSelectedLeft(Element icon) {
        this.mIconSelectedLeft = icon;
        this.mIconLeft = icon;
        setIcon();
        return this;
    }

    /**
     * 选中的置于右边图片
     *
     * @param icon
     * @return 图片信息
     */
    public RTextViewHelper setIconSelectedRight(Element icon) {
        this.mIconSelectedRight = icon;
        this.mIconRight = icon;
        setIcon();
        return this;
    }

    /**
     * 选中的置于上方图片
     *
     * @param icon
     * @return 图片信息
     */
    public RTextViewHelper setIconSelectedTop(Element icon) {
        this.mIconSelectedTop = icon;
        this.mIconTop = icon;
        setIcon();
        return this;
    }

    /**
     * 选中的图片置于下面
     *
     * @param icon
     * @return 图片
     */
    public RTextViewHelper setIconSelectedBottom(Element icon) {
        this.mIconSelectedBottom = icon;
        this.mIconBottom = icon;
        setIcon();
        return this;
    }

    public Element getIconSelectedLeft() {
        return mIconSelectedLeft;
    }

    public Element getIconSelectedRight() {
        return mIconSelectedRight;
    }

    public Element getIconSelectedTop() {
        return mIconSelectedTop;
    }

    public Element getIconSelectedBottom() {
        return mIconSelectedBottom;
    }

    /**
     * 图片大小左边
     *
     * @param iconWidth
     * @param iconHeight
     * @return 图片信息
     */
    public RTextViewHelper setIconSizeLeft(int iconWidth, int iconHeight) {
        this.mIconWidthLeft = iconWidth;
        this.mIconHeightLeft = iconHeight;
        setIcon();
        return this;
    }

    /**
     * 右边图片大小
     *
     * @param iconWidth
     * @param iconHeight
     * @return 图片信息
     */
    public RTextViewHelper setIconSizeRight(int iconWidth, int iconHeight) {
        this.mIconWidthRight = iconWidth;
        this.mIconHeightRight = iconHeight;
        setIcon();
        return this;
    }

    /**
     * 图片大小上面
     *
     * @param iconWidth
     * @param iconHeight
     * @return 图片信息
     */
    public RTextViewHelper setIconSizeTop(int iconWidth, int iconHeight) {
        this.mIconWidthTop = iconWidth;
        this.mIconHeightTop = iconHeight;
        setIcon();
        return this;
    }

    /**
     * 设置图片的大小
     *
     * @param iconWidth
     * @param iconHeight
     * @return 图片信息
     */
    public RTextViewHelper setIconSizeBottom(int iconWidth, int iconHeight) {
        this.mIconWidthBottom = iconWidth;
        this.mIconHeightBottom = iconHeight;
        setIcon();
        return this;
    }

    public int getIconWidthLeft() {
        return mIconWidthLeft;
    }

    public int getIconHeightLeft() {
        return mIconHeightLeft;
    }

    public int getIconWidthRight() {
        return mIconWidthRight;
    }

    public int getIconHeightRight() {
        return mIconHeightRight;
    }

    public int getIconWidthTop() {
        return mIconWidthTop;
    }

    public int getIconHeightTop() {
        return mIconHeightTop;
    }

    public int getIconWidthBottom() {
        return mIconWidthBottom;
    }

    public int getIconHeightBottom() {
        return mIconHeightBottom;
    }

    /**
     * 主要用于子类调用
     *
     * @param element 背景
     */
    @Deprecated
    @SuppressWarnings("unchecked")
    protected void setIcon(Element element) {
        this.mIcon = element;
        setIcon();
    }

    /**
     * 主要用于子类调用
     *
     * @param element 背景
     */
    @SuppressWarnings("unchecked")
    protected void setIconLeft(Element element) {
        this.mIconLeft = element;
        setIcon();
    }

    /**
     * 右边图
     *
     * @param icon
     */
    @SuppressWarnings("unchecked")
    protected void setIconRight(Element icon) {
        this.mIconRight = icon;
        setIcon();
    }

    @SuppressWarnings("unchecked")
    protected void setIconTop(Element icon) {
        this.mIconTop = icon;
        setIcon();
    }

    @SuppressWarnings("unchecked")
    protected void setIconBottom(Element icon) {
        this.mIconBottom = icon;
        setIcon();
    }

    private void setIcon() {
        mView.invalidate();
        // 未设置图片大小
        if (mIconHeightLeft == 0 && mIconWidthLeft == 0) {
            if (mIconLeft != null) {
                mIconWidthLeft = mIconLeft.getWidth();
                mIconHeightLeft = mIconLeft.getHeight();
            }
        }
        if (mIconHeightRight == 0 && mIconWidthRight == 0) {
            if (mIconRight != null) {
                mIconWidthRight = mIconRight.getWidth();
                mIconHeightRight = mIconRight.getHeight();
            }
        }
        if (mIconHeightTop == 0 && mIconWidthTop == 0) {
            if (mIconTop != null) {
                mIconWidthTop = mIconTop.getWidth();
                mIconHeightTop = mIconTop.getHeight();
            }
        }
        if (mIconHeightBottom == 0 && mIconWidthBottom == 0) {
            if (mIconBottom != null) {
                mIconWidthBottom = mIconBottom.getWidth();
                mIconHeightBottom = mIconBottom.getHeight();
            }
        }

        // 版本兼容代码
        if (mIconHeight == 0 && mIconWidth == 0) {
            if (mIcon != null) {
                mIconWidth = mIcon.getWidth();
                mIconHeight = mIcon.getHeight();
            }
        }

        setIcon(mIconLeft, mIconRight, mIconTop, mIconBottom);
    }

    /**
     * 新版本设置icon逻辑代码
     *
     * @param drawableLeft
     * @param drawableRight
     * @param drawableTop
     * @param drawableBottom
     */
    private void setIcon(Element drawableLeft, Element drawableRight, Element drawableTop, Element drawableBottom) {
        if (drawableLeft != null || drawableRight != null || drawableTop != null || drawableBottom != null) {
            if (drawableLeft != null) {
                drawableLeft.setBounds(0, 0, mIconWidthLeft, mIconHeightLeft);
            }
            if (drawableRight != null) {
                drawableRight.setBounds(0, 0, mIconWidthRight, mIconHeightRight);
            }
            if (drawableTop != null) {
                drawableTop.setBounds(0, 0, mIconWidthTop, mIconHeightTop);
            }
            if (drawableBottom != null) {
                drawableBottom.setBounds(0, 0, mIconWidthBottom, mIconHeightBottom);
            }

            // setDrawable
            mView.setAroundElements(drawableLeft, drawableTop, drawableRight, drawableBottom);

            // drawable间距
            if (!isDrawableWithText) {
                return;
            }
            int drawablePadding = mView.getAroundElementsPadding();
            int drawablePaddingHorizontal = 0, drawablePaddingVertical = 0;
            if (mIconLeft != null) {
                drawablePaddingHorizontal += drawablePadding;
            }
            if (mIconRight != null) {
                drawablePaddingHorizontal += drawablePadding;
            }
            if (mIconTop != null) {
                drawablePaddingVertical += drawablePadding;
            }
            if (mIconBottom != null) {
                drawablePaddingVertical += drawablePadding;
            }
            final int drawableWidthFinal = mIconWidthLeft + mIconWidthRight;
            final int drawableHeightFinal = mIconHeightTop + mIconHeightBottom;
            final int drawablePaddingVerticalFinal = drawablePaddingVertical; // 垂直方向上drawable间距
            final int drawablePaddingHorizontalFinal = drawablePaddingHorizontal; // 水平方向上drawable间距
            eventHandler.postTask(new Runnable() {
                @Override
                public void run() {
                    // 水平方向计算
                    float textWidth = TextViewUtils.get().getTextWidth(mView, drawableWidthFinal, mPaddingLeft, mPaddingRight, drawablePaddingHorizontalFinal);
                    float bodyWidth = textWidth + drawableWidthFinal + drawablePaddingHorizontalFinal + 200; // 内容宽度
                    float actualWidth = mView.getWidth() - (mPaddingLeft + mPaddingRight); // 实际可用宽度
                    int translateX = (int) (actualWidth - bodyWidth) / 2; // 两边使用
                    if (0 > translateX) {
                        translateX = 0;
                    }

                    // 垂直方向计算
                    float textHeight = TextViewUtils.get().getTextHeight(mView, drawableHeightFinal,
                            mPaddingTop, mPaddingBottom, drawablePaddingVerticalFinal);
                    float bodyHeight = textHeight + drawableHeightFinal + drawablePaddingVerticalFinal; // 内容高度
                    float actualHeight = mView.getHeight() - (mPaddingTop + mPaddingBottom); // 实际可用高度
                    int translateY = (int) (actualHeight - bodyHeight) / 2;
                    if (0 > translateY) {
                        translateY = 0;
                    }

                    // 关键技术点
                    mView.setPadding(translateX + mPaddingLeft, translateY + mPaddingTop, translateX + mPaddingRight, translateY + mPaddingBottom);
                }
            }, 2000);
        }
    }

    /**
     * 默认字体颜色
     *
     * @param textColor
     * @return 字体颜色
     */
    public RTextViewHelper setTextColorNormal(Color textColor) {
        this.mTextColorNormal = textColor;
        if (!isHasPressedTextColor) {
            mTextColorPressed = mTextColorNormal;
        }
        if (!isHasUnableTextColor) {
            mTextColorUnable = mTextColorNormal;
        }
        if (!isHasSelectedTextColor) {
            mTextColorSelected = mTextColorNormal;
        }
        setTextColor();
        return this;
    }

    public Color getTextColorNormal() {
        return mTextColorNormal;
    }

    /**
     * 点下后的字体颜色
     *
     * @param textColor
     * @return 颜色
     */
    public RTextViewHelper setTextColorPressed(Color textColor) {
        this.mTextColorPressed = textColor;
        this.isHasPressedTextColor = true;
        setTextColor();
        return this;
    }

    public Color getTextColorPressed() {
        return mTextColorPressed;
    }

    /**
     * 字体颜色
     *
     * @param textColor
     * @return 字体颜色
     */
    public RTextViewHelper setTextColorUnable(Color textColor) {
        this.mTextColorUnable = textColor;
        this.isHasUnableTextColor = true;
        setTextColor();
        return this;
    }

    public Color getTextColorUnable() {
        return mTextColorUnable;
    }

    /**
     * 子体选中后的颜色
     *
     * @param textColor
     * @return 颜色
     */
    public RTextViewHelper setTextColorSelected(Color textColor) {
        this.mTextColorSelected = textColor;
        this.isHasSelectedTextColor = true;
        setTextColor();
        return this;
    }

    public Color getTextColorSelected() {
        return mTextColorSelected;
    }

    /**
     * 颜色
     *
     * @param normal
     * @param pressed
     * @param unable
     * @param selected
     * @return 默认 点击后 选中颜色
     */
    public RTextViewHelper setTextColor(Color normal, Color pressed, Color unable, Color selected) {
        this.mTextColorNormal = normal;
        this.mTextColorPressed = pressed;
        this.mTextColorUnable = unable;
        this.mTextColorSelected = selected;
        this.isHasPressedTextColor = true;
        this.isHasUnableTextColor = true;
        this.isHasSelectedTextColor = true;
        setTextColor();
        return this;
    }

    /**
     * 设置颜色
     */
    protected void setTextColor() {
        // unable,focused,pressed,selected,normal
        Color[] colors = new Color[]{mTextColorUnable, mTextColorPressed,
                mTextColorPressed, mTextColorSelected, mTextColorNormal};

        ShapeElement element = new ShapeElement();

        mView.setTextColor(mTextColorNormal);
    }

    /**
     * 是否仅支持单一方向ICON
     * 老版本仅支持单一方向icon，版本兼容判断逻辑
     *
     * @return true 不为空反之
     */
    @SuppressWarnings("unchecked")
    protected boolean isSingleDirection() {
        // 老版本属性任意一个不为空，默认用户使用老版本逻辑，新版本只能使用带方向后缀的属性字段
        if (mIconNormal != null || mIconPressed != null || mIconUnable != null || mIconSelected != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 初始化按下状态文本颜色
     *
     * @param isCheckedTextColor 是否是选中的text颜色
     * @param textColorChecked 颜色
     */
    protected void initPressedTextColor(boolean isCheckedTextColor, Color textColorChecked) {
        if (!isHasPressedTextColor) {
            mTextColorPressed = isCheckedTextColor ? textColorChecked : mTextColorNormal;
        }
    }

    /**
     * 设置是否启用
     *
     * @param isEnabled true 是，反之
     */
    @Override
    @SuppressWarnings("unchecked")
    public void setEnabled(boolean isEnabled) {
        if (isEnabled) {
            if (mIconNormalLeft != null) {
                mIconLeft = mIconNormalLeft;
            }
            if (mIconNormalRight != null) {
                mIconRight = mIconNormalRight;
            }
            if (mIconNormalTop != null) {
                mIconTop = mIconNormalTop;
            }
            if (mIconNormalBottom != null) {
                mIconBottom = mIconNormalBottom;
            }
            if (mIconNormal != null) {
                mIcon = mIconNormal;
            }
            setIcon();
        } else {
            if (mIconUnableLeft != null) {
                mIconLeft = mIconUnableLeft;
            }
            if (mIconUnableRight != null) {
                mIconRight = mIconUnableRight;
            }
            if (mIconUnableTop != null) {
                mIconTop = mIconUnableTop;
            }
            if (mIconUnableBottom != null) {
                mIconBottom = mIconUnableBottom;
            }
            if (mIconUnable != null) {
                mIcon = mIconUnable;
            }
            setIcon();
        }
    }

    /**
     * 设置是否选中
     *
     * @param isSelected true 是，反之
     */
    @Override
    @SuppressWarnings("unchecked")
    public void setSelected(boolean isSelected) {
        if (!mView.isEnabled()) {
            return;
        }
        if (isSelected) {
            if (mIconSelectedLeft != null) {
                mIconLeft = mIconSelectedLeft;
            }
            if (mIconSelectedRight != null) {
                mIconRight = mIconSelectedRight;
            }
            if (mIconSelectedTop != null) {
                mIconTop = mIconSelectedTop;
            }
            if (mIconSelectedBottom != null) {
                mIconBottom = mIconSelectedBottom;
            }
            if (mIconSelected != null) {
                mIcon = mIconSelected;
            }
            setIcon();
        } else {
            if (mIconNormalLeft != null) {
                mIconLeft = mIconNormalLeft;
            }
            if (mIconNormalRight != null) {
                mIconRight = mIconNormalRight;
            }
            if (mIconNormalTop != null) {
                mIconTop = mIconNormalTop;
            }
            if (mIconNormalBottom != null) {
                mIconBottom = mIconNormalBottom;
            }
            if (mIconNormal != null) {
                mIcon = mIconNormal;
            }
            setIcon();
        }
    }

    private boolean isTouch;

    /**
     * 触摸事件逻辑
     *
     * @param component 视图
     * @param event
     * @return true
     */
    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        if (mView.isEnabled()) {
            int action = event.getAction();
            switch (action) { // 抬起
                case TouchEvent.PRIMARY_POINT_UP:
                    // 父级控件获取控制权
                    if (mIconNormalLeft != null) {
                        mIconLeft = mView.isSelected() ? mIconSelectedLeft : mIconNormalLeft;
                    }
                    if (mIconNormalRight != null) {
                        mIconRight = mView.isSelected() ? mIconSelectedRight : mIconNormalRight;
                    }
                    if (mIconNormalTop != null) {
                        mIconTop = mView.isSelected() ? mIconSelectedTop : mIconNormalTop;
                    }
                    if (mIconNormalBottom != null) {
                        mIconBottom = mView.isSelected() ? mIconSelectedBottom : mIconNormalBottom;
                    }
                    if (mIconNormal != null) {
                        mIcon = mView.isSelected() ? mIconSelected : mIconNormal;
                    }
                    if (isRipple && mView.isEnabled()) {
                        isTouch = true;
                        paint.setAlpha(0f);
                    }
                    mView.setTextColor(mTextColorNormal);

                    setIcon();
                    break;

                case TouchEvent.POINT_MOVE: // 移动
                    if (mIconNormalTop != null) {
                        mIconTop = mIconNormalTop;
                    }
                    mView.setTextColor(mTextColorNormal);
                    setIcon();
                    break;
                default:
                    break;
            }
            if (action == TouchEvent.PRIMARY_POINT_DOWN) {
                if (mIconPressedLeft != null) {
                    mIconLeft = mIconPressedLeft;
                }
                if (mIconPressedRight != null) {
                    mIconRight = mIconPressedRight;
                }
                if (mIconPressedTop != null) {
                    mIconTop = mIconPressedTop;
                }
                if (mIconPressedBottom != null) {
                    mIconBottom = mIconPressedBottom;
                }
                if (mIconPressed != null) {
                    mIcon = mIconPressed;
                }
                if (isRipple && mView.isEnabled()) {
                    pointerPosition = event.getPointerPosition(0);
                    float px = pointerPosition.getX();
                    float py = pointerPosition.getY();
                    int[] parentLocationOnScreen = mView.getLocationOnScreen();
                    int yy = (int) (py - parentLocationOnScreen[1]);
                    sx = component.getWidth()/2;
                    sy = component.getHeight()/ 2;
                    animatorValue.start();
                    isTouch = false;
                }
                mView.setTextColor(mTextColorPressed);
                setIcon();
                return true;
            }
        }

        return false;
    }

    /**
     * 可见性变化时更新text icon与文本一起居中
     *
     * @param changedView 视图
     * @param visibility 是否显示
     */
    @Override
    @SuppressWarnings("unchecked")
    public void onVisibilityChanged(Component changedView, int visibility) {
        if (visibility != Component.HIDE) {
            setIcon();
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (isRipple && mView.isEnabled()) {
            animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float value) {
                    zj = value;
                    if (value > 0.85 && isTouch) {
                        paint.setAlpha(0f);
                    } else {
                        paint.setAlpha(0.2f);
                    }
                    mView.invalidate();
                }
            });
            if (mView.getWidth() > mView.getHeight()) {
                canvas.drawCircle(sx, sy, mView.getWidth() * zj, paint);
            } else {
                canvas.drawCircle(sx, sy, mView.getHeight() * zj, paint);
            }
        }
    }

    @Override
    public void onRefreshed(Component component) {
        super.onRefreshed(component);
        if (isDrawableWithText) {
            mPaddingLeft = mView.getPaddingLeft();
            mPaddingRight = mView.getPaddingRight();
            mPaddingTop = mView.getPaddingTop();
            mPaddingBottom = mView.getPaddingBottom();
            setIcon();
        }
    }
}
