/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.org.matomo.demo.chechbox;

import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.render.BlurDrawLooper;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;

/**
 * Shadow Drawable
 *
 * @author ZhongDaFeng
 */
public class ShadowDrawable extends Element implements Component.DrawTask {
    private float mShadowRadius;//阴影半径
    private float[] mRoundRadii;//矩形圆角半径
    private int mShadowDx;//阴影x偏移
    private int mShadowDy;//阴影y偏移

    private Path mPath;
    private Paint mPaint;
    private RectFloat mBoundsF;

    public ShadowDrawable() {
        mPath = new Path();
        mBoundsF = new RectFloat();
        mPaint = new Paint();
    }

    public void updateParameter(Color shadowColor, float shadowRadius, int shadowDx, int shadowDy, float[] roundRadii) {
        //阴影颜色
        this.mRoundRadii = roundRadii;
        this.mShadowRadius = shadowRadius;
        this.mShadowDx = shadowDx;
        this.mShadowDy = shadowDy;
        mPaint.setColor(shadowColor);
        BlurDrawLooper blurDrawLooper = new BlurDrawLooper(mShadowRadius,mShadowDx, mShadowDy, shadowColor);
        mPaint.setBlurDrawLooper(blurDrawLooper);
    }

    @Override
    public void setBounds(Rect bounds) {
        super.setBounds(bounds);
        if (bounds.right - bounds.left > 0 && bounds.bottom - bounds.top > 0) {
            updateBounds(bounds);
        }
    }

    /**
     * 更新Bounds
     *
     * @param bounds 矩形对象
     */
    private void updateBounds(Rect bounds) {
        if (bounds == null) bounds = getBounds();
        /* float left = bounds.left + getShadowOffsetHalf() + offset;
        left = mShadowDx < 0 ? left + Math.abs(mShadowDx) : left;

        float right = bounds.right - getShadowOffsetHalf() - offset;
        right = mShadowDx > 0 ? right - Math.abs(mShadowDx) : right;

        float top = bounds.top + getShadowOffsetHalf() + offset;
        top = mShadowDy < 0 ? top + Math.abs(mShadowDy) : top;

        float bottom = bounds.bottom - getShadowOffsetHalf() - offset;
        bottom = mShadowDy > 0 ? bottom - Math.abs(mShadowDy) : bottom;*/

        float left = bounds.left + getShadowOffset() + Math.abs(mShadowDx);
        float right = bounds.right - getShadowOffset() - Math.abs(mShadowDx);
        float top = bounds.top + getShadowOffset() + Math.abs(mShadowDy);
        float bottom = bounds.bottom - getShadowOffset() - Math.abs(mShadowDy);

        mBoundsF.fuse(left, top, right, bottom);
        mPath.reset();//must重置
        mPath.addRoundRect(mBoundsF, mRoundRadii, Path.Direction.CLOCK_WISE);
    }

    @Override
    public void setAlpha(int alpha) {
        mPaint.setAlpha(alpha);
    }

    public float getShadowOffset() {
        return mShadowRadius;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.drawPath(mPath, mPaint);
    }
}
