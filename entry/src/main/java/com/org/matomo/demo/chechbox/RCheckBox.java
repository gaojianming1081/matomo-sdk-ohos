package com.org.matomo.demo.chechbox;
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import ohos.agp.components.AttrSet;
import ohos.agp.components.Checkbox;
import ohos.agp.components.Component;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

/**
 * RCheckBox
 *
 * @author ZhongDaFeng
 */
public class RCheckBox extends Checkbox implements RHelper<RCheckHelper>, Component.TouchEventListener {
    private RCheckHelper mHelper;

    public RCheckBox(Context context) {
        this(context, null);
    }

    public RCheckBox(Context context, AttrSet attrs) {
        super(context, attrs);
        mHelper = new RCheckHelper(context, this, attrs);
    }

    @Override
    public RCheckHelper getHelper() {
        return mHelper;
    }

    @Override
    public void setEnabled(boolean isEnabled) {
        super.setEnabled(isEnabled);
        if (mHelper != null) mHelper.setEnabled(isEnabled);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        if (mHelper != null) mHelper.onTouchEvent(component, event);
        return super.getTouchEventListener().onTouchEvent(component, event);
    }

    @Override
    public void setSelected(boolean isSelected) {
        if (mHelper != null) mHelper.setSelected(isSelected);
        super.setSelected(isSelected);
    }

    @Override
    public void setChecked(boolean isChecked) {
        if (mHelper != null) mHelper.setChecked(isChecked);
        super.setChecked(isChecked);
    }
}
