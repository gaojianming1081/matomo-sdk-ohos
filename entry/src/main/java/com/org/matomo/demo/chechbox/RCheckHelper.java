/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.org.matomo.demo.chechbox;

import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.app.Context;

import ohos.multimodalinput.event.TouchEvent;

/**
 * Check-Helper
 *
 * @author ZhongDaFeng
 */
public class RCheckHelper extends RTextViewHelper implements Component.TouchEventListener {
    // Text
    private Color mTextColorChecked;
    private boolean isHasCheckedTextColor = false;
    //Icon
    private Element mIconCheckedLeft;
    private Element mIconCheckedRight;
    private Element mIconCheckedTop;
    private Element mIconCheckedBottom;
    //版本兼容
    private Element mIconChecked;
    private Element checkedIdLeft = null;
    private Element checkedIdRight = null;
    private Element checkedIdTop = null;
    private Element checkedIdBottom = null;
    private Element checkedId = null;

    {
        //先于构造函数重置
        states = new int[6][];
    }

    /*
     * 提交
     *
     * */
    public RCheckHelper(Context context, Component view, AttrSet attrs) {
        super(context, (Text) view, attrs);
        initAttributeSet(context, attrs);
    }

    /**
     * 初始化控件属性
     *
     * @param context 上下文
     * @param attrs AttrSet
     */
    private void initAttributeSet(Context context, AttrSet attrs) {
        if (context == null || attrs == null) {
            setup();
            return;
        }

        Element a = TypedAttrUtils.getDrawable(attrs, "RRadioButton", null);
        //text
        mTextColorChecked = AttrUtils.getColor(attrs, "text_color_checked", null);

        //icon
        //Vector兼容处理

        mIconCheckedLeft = TypedAttrUtils.getDrawable(attrs, "icon_checked_left", null);
        mIconCheckedRight = TypedAttrUtils.getDrawable(attrs, "icon_checked_right", null);
        mIconCheckedTop = TypedAttrUtils.getDrawable(attrs, "icon_checked_top", null);
        mIconCheckedBottom = TypedAttrUtils.getDrawable(attrs, "icon_checked_bottom", null);
        mIconChecked = TypedAttrUtils.getDrawable(attrs, "icon_src_checked", null);

        checkedIdLeft = TypedAttrUtils.getDrawable(attrs, "icon_checked_left", checkedIdLeft);
        checkedIdRight = TypedAttrUtils.getDrawable(attrs, "icon_checked_right", checkedIdRight);
        checkedIdTop = TypedAttrUtils.getDrawable(attrs, "icon_checked_top", checkedIdTop);
        checkedIdBottom = TypedAttrUtils.getDrawable(attrs, "icon_checked_bottom", checkedIdBottom);
        if (checkedIdLeft != null) {
            mIconCheckedLeft = checkedIdLeft;
        }

        if (checkedIdRight != null)
            mIconCheckedRight = checkedIdRight;
        if (checkedIdTop != null)
            mIconCheckedTop = checkedIdTop;
        if (checkedIdBottom != null)
            mIconCheckedBottom = checkedIdBottom;
        checkedId = TypedAttrUtils.getDrawable(attrs, "icon_src_checked", checkedId);
        if (checkedId != null)
            mIconChecked = checkedId;
        isHasCheckedTextColor = mTextColorChecked != null;
        setup();
    }

    /*
     *
     * 设置
     *
     * @param
     */
    private void setup() {
        if (isChecked()) {
            setIconLeft(mIconCheckedLeft);
            setIconRight(mIconCheckedRight);
            setIconTop(mIconCheckedTop);
            setIconBottom(mIconCheckedBottom);
            setIcon(mIconChecked);
        }
        if (!isHasCheckedTextColor) {
            mTextColorChecked = mTextColorNormal;
        }
        initPressedTextColor(isHasCheckedTextColor, mTextColorChecked);

        //unable,focused,pressed,checked,selected,normal

        states[0] = new int[]{-ComponentState.COMPONENT_STATE_DISABLED};//unable

        states[1] = new int[]{ComponentState.COMPONENT_STATE_FOCUSED};//focused

        states[2] = new int[]{ComponentState.COMPONENT_STATE_PRESSED};//pressed

        states[3] = new int[]{ComponentState.COMPONENT_STATE_CHECKED};//checked

        states[4] = new int[]{ComponentState.COMPONENT_STATE_SELECTED};//selected

        states[5] = new int[]{ComponentState.COMPONENT_STATE_EMPTY};//normal

        //设置文本颜色
        setTextColor();
    }

    @Override
    public RCheckHelper setTextColorNormal(Color textColor) {
        if (!isHasCheckedTextColor) {
            mTextColorChecked = textColor;
        }
        super.setTextColorNormal(textColor);
        initPressedTextColor(isHasCheckedTextColor, mTextColorChecked);
        setTextColor();
        return this;
    }

    public RCheckHelper setTextColorChecked(Color textColor) {
        this.mTextColorChecked = textColor;
        this.isHasCheckedTextColor = true;
        initPressedTextColor(isHasCheckedTextColor, mTextColorChecked);
        setTextColor();
        return this;
    }

    public Color getTextColorChecked() {
        return mTextColorChecked;
    }

    public RCheckHelper setTextColor(Color normal, Color pressed, Color unable, Color checked, Color selected) {
        this.mTextColorChecked = checked;
        this.isHasCheckedTextColor = true;
        super.setTextColor(normal, pressed, unable, selected);
        return this;
    }

    @Override
    protected void setTextColor() {
        //unable,focused,pressed,checked,selected,normal
        Color[] colors = new Color[]{mTextColorUnable, mTextColorPressed, mTextColorPressed, mTextColorChecked, mTextColorSelected, mTextColorNormal};
        // mTextColorStateList = new ColorStateList(states, colors);
        mView.setTextColor(mTextColorNormal);
    }

    @Deprecated
    public RCheckHelper setIconChecked(Element icon) {
        this.mIconChecked = icon;
        setIcon(icon);
        return this;
    }

    @Deprecated
    public Element getIconChecked() {
        return mIconChecked;
    }

//*
//     * *********新版本逻辑*********
//

    public RCheckHelper setIconCheckedLeft(Element icon) {
        this.mIconCheckedLeft = icon;
        setIconLeft(icon);
        return this;
    }

    public RCheckHelper setIconCheckedRight(Element icon) {
        this.mIconCheckedRight = icon;
        setIconRight(icon);
        return this;
    }

    public RCheckHelper setIconCheckedTop(Element icon) {
        this.mIconCheckedTop = icon;
        setIconTop(icon);
        return this;
    }

    public RCheckHelper setIconCheckedBottom(Element icon) {
        this.mIconCheckedBottom = icon;
        setIconBottom(icon);
        return this;
    }

    public Element getIconCheckedLeft() {
        return mIconCheckedLeft;
    }

    public Element getIconCheckedRight() {
        return mIconCheckedRight;
    }

    public Element getIconCheckedTop() {
        return mIconCheckedTop;
    }

    public Element getIconCheckedBottom() {
        return mIconCheckedBottom;
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (!isChecked()) {
            super.onTouchEvent(component, touchEvent);
        }
        return false;
    }

    private boolean isChecked() {
        if (mView != null) {
            return ((AbsButton) mView).isChecked();
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    public void setChecked(boolean isChecked) {
        setIconLeft(isChecked ? mIconCheckedLeft : getIconNormalLeft());
        setIconRight(isChecked ? mIconCheckedRight : getIconNormalRight());
        setIconTop(isChecked ? mIconCheckedTop : getIconNormalTop());
        setIconBottom(isChecked ? mIconCheckedBottom : getIconNormalBottom());
        setIcon(isChecked ? mIconChecked : getIconNormal());
    }

    @Override
    protected boolean isSingleDirection() {
        return super.isSingleDirection() || (mIconChecked != null);
    }
}
