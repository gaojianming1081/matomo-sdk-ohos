/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.org.matomo.demo.button;

import com.org.matomo.demo.chechbox.RHelper;
import com.org.matomo.demo.chechbox.RTextViewHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Text;
import ohos.app.Context;

/**
 * RTextView
 *
 * @author ZhongDaFeng
 */
public class RTextView extends Text implements RHelper<RTextViewHelper> {
    private RTextViewHelper mHelper;

    public RTextView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    public RTextView(Context context) {
        this(context, null);
    }

    public RTextView(Context context, AttrSet attrs) {
        super(context, attrs);
        mHelper = new RTextViewHelper(context, this, attrs);
    }

    @Override
    public RTextViewHelper getHelper() {
        return mHelper;
    }

    @Override
    public void setEnabled(boolean isEnabled) {
        super.setEnabled(isEnabled);
        if (mHelper != null) mHelper.setEnabled(isEnabled);
    }

    @Override
    public void setSelected(boolean isSelected) {
        if (mHelper != null) mHelper.setSelected(isSelected);
        super.setSelected(isSelected);
    }

    @Override
    public boolean isTextCursorVisible() {
        return super.isTextCursorVisible();
    }

    @Override
    public void setVisibility(int visibility) {
        if (mHelper != null) mHelper.onVisibilityChanged(null, visibility);
        super.setVisibility(visibility);
    }
}
