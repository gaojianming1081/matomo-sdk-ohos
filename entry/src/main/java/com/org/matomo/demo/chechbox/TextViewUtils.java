/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.org.matomo.demo.chechbox;

import ohos.agp.components.Text;
import ohos.agp.render.Paint;

import java.util.ArrayList;
import java.util.Collections;

/**
 * TextView工具类
 *
 */
public class TextViewUtils {
    private static TextViewUtils instance = null;
    private Paint paint;

    private TextViewUtils() {
    }

    /**
     * 获取工具类
     *
     * @return 单列
     */
    public static TextViewUtils get() {
        if (instance == null) {
            synchronized (TextViewUtils.class) {
                if (instance == null) {
                    instance = new TextViewUtils();
                }
            }
        }
        return instance;
    }

    /**
     * 获取Text实际宽度
     * 备注:单行最大宽度
     *
     * @param view text
     * @param drawableWidth drawable宽度
     * @param paddingLeft 内边距-左
     * @param paddingRight 内边距-右
     * @param drawablePaddingHorizontal 水平方向上drawable间距
     * @return 宽度
     */
    public float getTextWidth(Text view, int drawableWidth, int paddingLeft,
                              int paddingRight, int drawablePaddingHorizontal) {
        if (view == null) {
            return 0;
        }
        paint = new Paint();
        float textWidth;
        /**
         * 1.是否存在\n换行获取宽度最大的值
         * 2.不存在\n获取单行宽度值判断是否自动换行临界值
         */
        String originalText = view.getText();
        if (originalText.contains("\n")) {
            String[] originalTextArray;
            ArrayList<Float> widthList;
            originalTextArray = originalText.split("\n");
            int arrayLen = originalTextArray.length;
            widthList = new ArrayList<>(arrayLen);
            for (int i = 0; i < arrayLen; i++) {
                widthList.add(paint.measureText(originalTextArray[i]));
            }
            textWidth = Collections.max(widthList);
        } else {
            textWidth = paint.measureText(originalText);
        }

        // 计算自动换行临界值，不允许超过换行临界值
        int maxWidth = view.getWidth() - drawableWidth - paddingLeft - paddingRight - drawablePaddingHorizontal;
        if (textWidth > maxWidth) {
            textWidth = maxWidth;
        }
        return textWidth;
    }

    /**
     * 获取Text实际高度
     * 备注:多行最大高度
     *
     * @param view text
     * @param drawableHeight drawable高度
     * @param paddingTop 内边距-左
     * @param paddingBottom 内边距-右
     * @param drawablePaddingVertical 垂直方向上drawabl e间距
     * @return 宽高
     */
    public float getTextHeight(Text view, int drawableHeight, int paddingTop, int paddingBottom, int drawablePaddingVertical) {
        if (view == null) {
            return 0;
        }

        /**
         * 1.单行高度*行数2.最大高度临界值
         */

        Paint paint = new Paint();
        // 单行高度
        float singleLineHeight = Math.abs(
                (paint.getFontMetrics().bottom - paint.getFontMetrics().top));
        float textHeight = singleLineHeight * view.getMaxTextLines();

        // 最大高度临界值，不允许超过最大高度临界值
        int maxHeight = view.getHeight() - drawableHeight - paddingTop
                - paddingBottom - drawablePaddingVertical; // 最大允许的宽度
        if (textHeight > maxHeight) {
            textHeight = maxHeight;
        }
        return textHeight;
    }
}
