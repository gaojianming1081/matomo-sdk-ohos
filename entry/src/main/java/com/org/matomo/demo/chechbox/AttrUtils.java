/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.org.matomo.demo.chechbox;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.NoSuchElementException;

public class AttrUtils {
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x12345, "yuki");

    public static boolean getBoolean(AttrSet attrs, String attrName, boolean isDefValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return isDefValue;
        } else {
            return attr.getBoolValue();
        }
    }

    public static String getString(AttrSet attrs, String attrName, String defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getStringValue();
        }
    }

    public static int getInt(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getIntegerValue();
        }
    }

    public static float getFloat(AttrSet attrs, String attrName, float defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getFloatValue();
        }
    }

    public static Element getElement(AttrSet attrs, String attrName, Element defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getElement();
        }
    }

    private static Attr attrNoSuchElement(AttrSet attrs, String attrName) {
        Attr attr = null;
        try {
            attr = attrs.getAttr(attrName).get();
        } catch (NoSuchElementException e) {
            HiLog.info(LABEL, "Exception = " + e.toString());
        }
        return attr;
    }

    /**
     * 获取getXmlManyValue
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return 获取xml里面传递的多个值（也就是数组）
     */
    public static String getXmlManyValue(AttrSet attrs, String attrName, String defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getStringValue();
        }
    }

    public static Color getColor(AttrSet attrs, String attrName, Color defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getColorValue();
        }
    }

    /**
     * string 颜色值 转换
     *
     * @param attrs AttrSet
     * @param attrName 名称
     * @param defValue 默认值
     * @return 颜色
     */
    public static Color getStringToColor(AttrSet attrs, String attrName, Color defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            String strColor = attr.getStringValue();
            return new Color(Color.getIntColor(strColor));
        }
    }

    /**
     * bool类型
     *
     * @param attrs AttrSet
     * @param attrName 名称
     * @param isDefValue 默认值
     * @return boolean
     */
    public static boolean getStringToBool(AttrSet attrs, String attrName, boolean isDefValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return isDefValue;
        } else {
            String stringBool = attr.getStringValue();
            return stringBool.toLowerCase().equals("true");
        }
    }
}
