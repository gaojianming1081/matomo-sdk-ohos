package com.org.matomo.demo;

import com.org.matomo.demo.slice.SettingAbliltySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class SettingAblilty extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(SettingAbliltySlice.class.getName());
    }
}
