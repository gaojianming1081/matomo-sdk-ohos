/*
 * Android SDK for Matomo
 *
 * @link https://github.com/matomo-org/matomo-android-sdk
 * @license https://github.com/matomo-org/matomo-sdk-android/blob/master/LICENSE BSD-3 Clause
 */

package org.matomo.sdk.extra;

import ohos.aafwk.ability.AbilityPackage;
import ohos.aafwk.ability.MemoryInfo;
import ohos.aafwk.ability.SystemMemoryInfo;
import ohos.aafwk.ability.ViewsStatus;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import org.matomo.sdk.Matomo;
import org.matomo.sdk.Tracker;
import org.matomo.sdk.TrackerBuilder;
import org.matomo.sdk.dispatcher.Event;
import org.matomo.sdk.dispatcher.EventDiskCache;

import javax.net.ssl.SSLEngineResult;
import java.util.logging.Level;
import java.util.logging.MemoryHandler;

public abstract class MatomoApplication extends AbilityPackage {
    private Tracker mMatomoTracker;

    public Matomo getMatomo() {
        return Matomo.getInstance(this);
    }

    /**
     * Gives you an all purpose thread-safe persisted Tracker.
     *
     * @return a shared Tracker
     */
    public synchronized Tracker getTracker() {
        if (mMatomoTracker == null) mMatomoTracker = onCreateTrackerConfig().build(getMatomo());
        return mMatomoTracker;
    }

    /**
     * See {@link TrackerBuilder}.
     * You may be interested in {@link TrackerBuilder#createDefault(String, int)}
     *
     * @return the tracker configuration you want to use.
     */
    public abstract TrackerBuilder onCreateTrackerConfig();

    @Override
    public void onMemoryLevel(int level) {
        SystemMemoryInfo systemMemoryInfo = new SystemMemoryInfo();
        boolean isLowSysMemory = systemMemoryInfo.isLowSysMemory();
        if (isLowSysMemory) {
            mMatomoTracker.dispatch();
        }
        // todo
        if (mMatomoTracker != null) {
            mMatomoTracker.dispatch();
        }
        super.onMemoryLevel(level);
    }
}
