package org.matomo.sdk.dispatcher;

public interface PacketSender {
    /**
     * 发送
     *
     * @param packet Packet
     * @return true if successful
     */
    boolean send(Packet packet);

    /**
     * 设置时间
     *
     * @param timeout in milliseconds
     */
    void setTimeout(long timeout);

    void setGzipData(boolean isGzip);
}
