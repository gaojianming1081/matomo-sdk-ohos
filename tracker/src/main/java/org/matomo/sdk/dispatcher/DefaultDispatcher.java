/*
 * Android SDK for Matomo
 *
 * @link https://github.com/matomo-org/matomo-android-sdk
 * @license https://github.com/matomo-org/matomo-sdk-android/blob/master/LICENSE BSD-3 Clause
 */

package org.matomo.sdk.dispatcher;

import org.matomo.sdk.Logger;
import org.matomo.sdk.Matomo;
import org.matomo.sdk.TrackMe;
import org.matomo.sdk.tools.Connectivity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * Responsible for transmitting packets to a server
 */
public class DefaultDispatcher implements Dispatcher {
    private static final String TAG = Matomo.tag(DefaultDispatcher.class);
    private final Object mThreadControl = new Object();
    private final EventCache mEventCache;
    private final Semaphore mSleepToken = new Semaphore(0);
    private final Connectivity mConnectivity;
    private final PacketFactory mPacketFactory;
    private final PacketSender mPacketSender;
    private volatile int mTimeOut = DEFAULT_CONNECTION_TIMEOUT;
    private volatile long mDispatchInterval = DEFAULT_DISPATCH_INTERVAL;
    private volatile int mRetryCounter = 0;
    private volatile boolean isForcedBlocking = false;

    private boolean isDispatchGzipped = false;
    private volatile DispatchMode mDispatchMode = DispatchMode.ALWAYS;
    private volatile boolean isRunning = false;
    private volatile Thread mDispatchThread = null;
    private List<Packet> mDryRunTarget = null;

    public DefaultDispatcher(EventCache eventCache, Connectivity connectivity, PacketFactory packetFactory, PacketSender packetSender) {
        mConnectivity = connectivity;
        mEventCache = eventCache;
        mPacketFactory = packetFactory;
        mPacketSender = packetSender;
        packetSender.setGzipData(isDispatchGzipped);
        packetSender.setTimeout(mTimeOut);
    }

    /**
     * Connection timeout in milliseconds
     *
     * @return timeout in milliseconds
     */
    @Override
    public int getConnectionTimeOut() {
        return mTimeOut;
    }

    /**
     * Timeout when trying to establish connection and when trying to read a response.
     * Values take effect on next dispatch.
     *
     * @param timeOut timeout in milliseconds
     */
    @Override
    public void setConnectionTimeOut(int timeOut) {
        mTimeOut = timeOut;
        mPacketSender.setTimeout(mTimeOut);
    }

    /**
     * Packets are collected and dispatched in batches, this intervals sets the pause between batches.
     *
     * @param dispatchInterval in milliseconds
     */
    @Override
    public void setDispatchInterval(long dispatchInterval) {
        System.out.println("xcccccc>>>>" + ">>>>>" + dispatchInterval);
        mDispatchInterval = dispatchInterval;
        if (mDispatchInterval != -1) launch();
    }

    @Override
    public long getDispatchInterval() {
        return mDispatchInterval;
    }

    /**
     * Packets are collected and dispatched in batches. This boolean sets if post must be
     * gzipped or not. Use of gzip needs mod_deflate/Apache ou lua_zlib/NGINX
     *
     * @param isDispatchGzipped boolean
     */
    @Override
    public void setDispatchGzipped(boolean isDispatchGzipped) {
        this.isDispatchGzipped = isDispatchGzipped;
        mPacketSender.setGzipData(isDispatchGzipped);
    }

    @Override
    public boolean getDispatchGzipped() {
        return isDispatchGzipped;
    }

    @Override
    public void setDispatchMode(DispatchMode dispatchMode) {
        this.mDispatchMode = dispatchMode;
    }

    @Override
    public DispatchMode getDispatchMode() {
        return mDispatchMode;
    }

    private boolean launch() {
        synchronized (mThreadControl) {
            if (isRunning) {
                isRunning = false;
            }
            if (!isRunning) {
                isRunning = true;
                Thread thread = new Thread(mLoop);
                thread.setPriority(Thread.MIN_PRIORITY);
                thread.setName("Matomo-default-dispatcher");
                mDispatchThread = thread;
                thread.start();
                return true;
            }
        }
        return false;
    }

    /**
     * Starts the dispatcher for one cycle if it is currently not working.
     * If the dispatcher is working it will skip the dispatch interval once.
     *
     * @return boolean
     */
    @Override
    public boolean forceDispatch() {
        if (!launch()) {
            mRetryCounter = 0;
            mSleepToken.release();
            return false;
        }
        return true;
    }

    @Override
    public void forceDispatchBlocking() {
        synchronized (mThreadControl) {
            // force thread to exit after it completes its dispatch loop
            isForcedBlocking = true;
        }

        if (forceDispatch()) {
            mSleepToken.release();
        }

        Thread dispatchThread = mDispatchThread;

        if (dispatchThread != null) {
            try {
                dispatchThread.join();
            } catch (InterruptedException e) {
                Logger.error("Interrupted while waiting for dispatch thread to complete");
            }
        }

        synchronized (mThreadControl) {
            // re-enable default behavior
            isForcedBlocking = false;
        }
    }

    @Override
    public void clear() {
        mEventCache.clear();
        // Try to exit the loop as the queue is empty
        if (isRunning) forceDispatch();
    }

    @Override
    public void submit(TrackMe trackMe) {
        mEventCache.add(new Event(trackMe.toMap()));
        if (mDispatchInterval != -1) launch();
    }

    private final Runnable mLoop = new Runnable() {
        @Override
        public void run() {
            mRetryCounter = 0;
            int count1 = 0;
            List<Event> drainedEvents1 = new ArrayList<>();
            mEventCache.drainTo(drainedEvents1);
            List<Packet> drainedEvents2 = mPacketFactory.buildPackets(drainedEvents1);
            if (drainedEvents2 != null && drainedEvents2.size() > 0) {
                System.out.println("injectBaseParams>>>>>" + ">>>>>" + "jin" + drainedEvents2.get(0).getPostData());
            }
            for (Packet packet : mPacketFactory.buildPackets(drainedEvents1)) {
                boolean success;
                if (mDryRunTarget != null) {
                    success = mDryRunTarget.add(packet);
                } else {
                    success = mPacketSender.send(packet);
                }

                if (success) {
                    count1 += packet.getEventCount();
                    mRetryCounter = 0;
                } else {
                    // On network failure, requeue all un-sent events, but use isOnline to determine if events should be cached in
                    // memory or disk
                    Logger.error("Failure while trying to send packet");
                    mRetryCounter++;
                    break;
                }

                // Re-check network connectivity to early exit if we drop offline.  This speeds up how quickly the setOffline method will
                // take effect
                if (!isOnline()) {
                    Logger.error("Disconnected during dispatch loop");
                    break;
                }
            }
            while (isRunning) {
                try {
                    long sleepTime = mDispatchInterval;
                    if (mRetryCounter > 1)
                        sleepTime += Math.min(mRetryCounter * mDispatchInterval, 5 * mDispatchInterval);

                    // Either we wait the interval or forceDispatch() granted us one free pass
                    mSleepToken.tryAcquire(sleepTime, TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                    Logger.error(e.getMessage());
                }
                if (mEventCache.updateState(isOnline())) {
                    int count = 0;
                    List<Event> drainedEvents = new ArrayList<>();
                    mEventCache.drainTo(drainedEvents);
                    Logger.error("Drained %s events." + drainedEvents.size());
                    for (Packet packet : mPacketFactory.buildPackets(drainedEvents)) {
                        boolean success;
                        if (mDryRunTarget != null) {
                            Logger.error("DryRun, stored HttpRequest, now %d." + mDryRunTarget.size());
                            success = mDryRunTarget.add(packet);
                        } else {
                            success = mPacketSender.send(packet);
                        }

                        if (success) {
                            count += packet.getEventCount();
                            mRetryCounter = 0;
                        } else {
                            // On network failure, requeue all un-sent events, but use isOnline to determine if events should be cached in
                            // memory or disk
                            Logger.error("Failure while trying to send packet");
                            mRetryCounter++;
                            break;
                        }
                        if (!isOnline()) {
                            Logger.error("Disconnected during dispatch loop");
                            break;
                        }
                    }
                    Logger.error("Dispatched %d events." + count);
                    if (count < drainedEvents.size()) {
                        Logger.error("Unable to send all events, requeueing %d events" + (drainedEvents.size() - count));
                        mEventCache.requeue(drainedEvents.subList(count, drainedEvents.size()));
                        mEventCache.updateState(isOnline());
                    }
                }

                synchronized (mThreadControl) {
                    if (isForcedBlocking || mEventCache.isEmpty() || mDispatchInterval < 0) {
                        isRunning = false;
                        break;
                    }
                }
            }
        }
    };

    private boolean isOnline() {
        if (!mConnectivity.isConnected()) return false;

        switch (mDispatchMode) {
            case EXCEPTION:
                return false;
            case ALWAYS:
                return true;
            case WIFI_ONLY:
                return mConnectivity.getType() == Connectivity.Type.WIFI;
        }
        return false;
    }

    @Override
    public void setDryRunTarget(List<Packet> dryRunTarget) {
        mDryRunTarget = dryRunTarget;
    }

    @Override
    public List<Packet> getDryRunTarget() {
        return mDryRunTarget;
    }
}
