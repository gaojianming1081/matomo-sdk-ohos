package org.matomo.sdk;

import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Configuration details for a {@link Tracker}
 */
public class TrackerBuilder {
    private String mApiUrl;
    private int mSiteId;
    private String mTrackerName;
    private String mApplicationBaseUrl;

    public static TrackerBuilder createDefault(String apiUrl, int siteId) {
        return new TrackerBuilder(apiUrl, siteId, "Default Tracker");
    }

    /**
     * 构造方法
     *
     * @param apiUrl      Tracking HTTP API endpoint, for example, https://matomo.yourdomain.tld/matomo.php
     * @param siteId      id of your site in the backend
     * @param trackerName name of your tracker, will be used to store configuration data
     */
    public TrackerBuilder(String apiUrl, int siteId, String trackerName) {
        try {
            new URL(apiUrl);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        mApiUrl = apiUrl;
        mSiteId = siteId;
        mTrackerName = trackerName;
    }

    public String getApiUrl() {
        return mApiUrl;
    }

    public int getSiteId() {
        return mSiteId;
    }

    /**
     * A unique name for this Tracker. Used to store Tracker settings independent of URL and id changes.
     *
     * @param name name
     * @return TrackerBuilder
     */
    public TrackerBuilder setTrackerName(String name) {
        mTrackerName = name;
        return this;
    }

    public String getTrackerName() {
        return mTrackerName;
    }

    /**
     * Domain used to build the required parameter url (http://developer.matomo.org/api-reference/tracking-api)
     * Defaults to`https://your.packagename`
     *
     * @param domain your-domain.com
     * @return TrackerBuilder
     */
    public TrackerBuilder setApplicationBaseUrl(String domain) {
        mApplicationBaseUrl = domain;
        return this;
    }

    public String getApplicationBaseUrl() {
        return mApplicationBaseUrl;
    }

    public Tracker build(Matomo matomo) {
        if (mApplicationBaseUrl == null) {
            String formatUrl = null;
            try {
                formatUrl = matomo.getContext().getResourceManager().getElement(ResourceTable.String_format_url).getString();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            } catch (WrongTypeException e) {
                e.printStackTrace();
            }
            mApplicationBaseUrl = String.format(formatUrl, matomo.getContext().getBundleName());
        }
        return new Tracker(matomo, this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TrackerBuilder that = (TrackerBuilder) o;

        return mSiteId == that.mSiteId && mApiUrl.equals(that.mApiUrl) && mTrackerName.equals(that.mTrackerName);
    }

    @Override
    public int hashCode() {
        int result = mApiUrl.hashCode();
        result = 31 * result + mSiteId;
        result = 31 * result + mTrackerName.hashCode();
        return result;
    }
}
