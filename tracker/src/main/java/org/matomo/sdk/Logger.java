/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.matomo.sdk;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class Logger {
    public static final HiLogLabel LABEL = new HiLogLabel(HiLog.INFO, 0x00201,
            "tracker");

    public static void debug(Object object) {
        HiLog.debug(LABEL, "debug%{public}s", object);
    }

    public static void info(Object object) {
        HiLog.debug(LABEL, "info%{public}s", object);
    }

    public static void warn(Object object) {
        HiLog.debug(LABEL, "warn%{public}s", object);
    }

    public static void error(Object object) {
        HiLog.debug(LABEL, "error%{public}s", object);
    }
}
