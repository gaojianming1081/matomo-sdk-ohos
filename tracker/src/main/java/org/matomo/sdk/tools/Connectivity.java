package org.matomo.sdk.tools;

import ohos.app.Context;
import ohos.net.NetHandle;
import ohos.net.NetManager;
import ohos.telephony.NetworkState;
import ohos.wifi.WifiDevice;

public class Connectivity {
    // 获取WLAN管理对象
    private WifiDevice wifiDevice;
    // 调用获取WLAN开关状态接口
    private boolean isWifiActive;
    private Context context;

    public Connectivity(Context context) {
        this.context = context;
        wifiDevice = WifiDevice.getInstance(context);
        isWifiActive = wifiDevice.isWifiActive(); // 若WLAN打开，则返回true，否则返回false
    }

    /**
     * 判断是否有网络连接
     *
     * @return true 连接
     */
    public boolean isConnected() {
        NetHandle[] allNets = NetManager.getInstance(context).getAllNets();
        return allNets != null && allNets.length > 0;
    }

    public enum Type {
        NONE, MOBILE, WIFI
    }

    public Type getType() {
        boolean isWifeConnect = wifiDevice.isConnected();
        if (isWifiActive && isWifeConnect) {
            return Type.WIFI;
        }
        if (isConnected()) {
            return Type.MOBILE;
        }
        return Type.NONE;
    }
}
