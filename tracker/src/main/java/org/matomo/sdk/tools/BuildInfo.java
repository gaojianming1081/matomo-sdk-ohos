package org.matomo.sdk.tools;

import ohos.aafwk.ability.DataAbilityHelper;
import ohos.app.Context;
import ohos.sysappcomponents.settings.SystemSettings;
import ohos.system.DeviceInfo;
import ohos.system.version.SystemVersion;
import org.matomo.sdk.BuildConfig;

public class BuildInfo {
    private final DataAbilityHelper dataAbilityHelper;

    public BuildInfo(Context context) {
        dataAbilityHelper = DataAbilityHelper.creator(context);
    }

    public String getRelease() {
        return String.valueOf(SystemVersion.getApiVersion());
    }

    public String getModel() {
        return SystemSettings.getValue(dataAbilityHelper, SystemSettings.General.DEVICE_NAME);
    }

    public String getBuildId() {
        return SystemSettings.getValue(dataAbilityHelper, SystemSettings.General.DEVICE_NAME);
    }
}
