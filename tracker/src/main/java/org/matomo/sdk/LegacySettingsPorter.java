package org.matomo.sdk;

import ohos.data.preferences.Preferences;

import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

public class LegacySettingsPorter {
    static final String LEGACY_PREF_OPT_OUT = "matomo.optout";
    static final String LEGACY_PREF_USER_ID = "tracker.userid";
    static final String LEGACY_PREF_FIRST_VISIT = "tracker.firstvisit";
    static final String LEGACY_PREF_VISITCOUNT = "tracker.visitcount";
    static final String LEGACY_PREF_PREV_VISIT = "tracker.previousvisit";
    private final Preferences mLegacyPrefs;

    public LegacySettingsPorter(Matomo matomo) {
        mLegacyPrefs = matomo.getPreferences();
    }

    public void port(Tracker tracker) {
        Preferences newSettings = tracker.getPreferences();
        if (mLegacyPrefs.getBoolean(LEGACY_PREF_OPT_OUT, false)) {
            newSettings.putBoolean(
                    Tracker.PREF_KEY_TRACKER_OPTOUT,
                    true
            ).flushSync();
            mLegacyPrefs.delete(LEGACY_PREF_OPT_OUT).flushSync();
        }

        if (mLegacyPrefs.hasKey(LEGACY_PREF_USER_ID)) {
            newSettings.putString(
                    Tracker.PREF_KEY_TRACKER_USERID,
                    mLegacyPrefs.getString(LEGACY_PREF_USER_ID, UUID.randomUUID().toString())
            ).flushSync();
            mLegacyPrefs.delete(LEGACY_PREF_USER_ID).flushSync();
        }
        if (mLegacyPrefs.hasKey(LEGACY_PREF_FIRST_VISIT)) {
            newSettings.putLong(
                    Tracker.PREF_KEY_TRACKER_FIRSTVISIT,
                    mLegacyPrefs.getLong(LEGACY_PREF_FIRST_VISIT, -1L)
            ).flushSync();
            mLegacyPrefs.delete(LEGACY_PREF_FIRST_VISIT).flushSync();
        }
        if (mLegacyPrefs.hasKey(LEGACY_PREF_VISITCOUNT)) {
            newSettings.putLong(
                    Tracker.PREF_KEY_TRACKER_VISITCOUNT,
                    mLegacyPrefs.getInt(LEGACY_PREF_VISITCOUNT, 0)
            ).flushSync();
            mLegacyPrefs.delete(LEGACY_PREF_VISITCOUNT).flushSync();
        }
        if (mLegacyPrefs.hasKey(LEGACY_PREF_PREV_VISIT)) {
            newSettings.putLong(
                    Tracker.PREF_KEY_TRACKER_PREVIOUSVISIT,
                    mLegacyPrefs.getLong(LEGACY_PREF_PREV_VISIT, -1)
            ).flushSync();
            mLegacyPrefs.delete(LEGACY_PREF_PREV_VISIT).flushSync();
        }
        final Iterator<? extends Map.Entry<String, ?>> it = mLegacyPrefs.getAll().entrySet().iterator();
        while (it.hasNext()) {
            final Map.Entry<String, ?> oldEntry = it.next();
            if (oldEntry.getKey().startsWith("downloaded:")) {
                newSettings.putBoolean(oldEntry.getKey(), true).flushSync();
                mLegacyPrefs.delete(oldEntry.getKey()).flushSync();
            }
        }
    }
}
