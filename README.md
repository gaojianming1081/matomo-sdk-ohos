# matomo-sdk-ohos
本项目是基于开源项目matomo-sdk-android进行openharmony的移植和开发的，可以通过项目地址(<https://github.com/matomo-org/matomo-sdk-android>)追踪到原项目版本

## 项目介绍
项目名称：打点数据上报

所属系列：openharmony的第三方组件适配移植

功能：可以解析每个按钮的点击事件所上报的数据

基线版本：matomo-sdk-ohos组件 Release版本号 v4.1.2

项目移植状态：有差异

调用差异：无

开发版本：sdk5，DevEco Studio2.1 beta3

项目作者和维护人：赵旭东

联系方式：zhaoxudong073@chinasoftinc.com

原项目Doc地址：<https://github.com/matomo-org/matomo-sdk-android>

编程语言：Java

外部库依赖：matomo-sdk-ohos项目中的tracker库


## 效果展示

<img src="img/demo.png"></img>

## 安装教程
1、在项目根目录下的build.gradle文件中
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
        }
    }
}
```
2.在entry模块的build.gradle文件中
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:matomo-sdk-ohos:0.0.2-SNAPSHOT')
    ......  
 }
```

在sdk5，DevEco Studio2.1 beta3下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下


## 使用说明

1.在AbilityPackage子类中初始化上报数据的基类和url

2.布局定义按钮

3.初始化按钮事件

4.在点击事件里面绑定上报数据

```
@Override
public TrackerBuilder onCreateTrackerConfig() {
    return TrackerBuilder.createDefault("https://demo.matomo.org/matomo.php", 53);
}
TrackHelper.track(new TrackMe().set(QueryParams.SESSION_START, 1))
        .screen("/")
        .title("Main screen")
        .with(getTracker()));
```

## 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

火绒安全病毒安全检测通过

当前版本demo功能与原组件基本无差异

测试员：郑冲

## 版本迭代
- 0.0.2-SNAPSHOT

## 版权和许可信息
Android SDK for Matomo is released under the BSD-3 Clause license, see [LICENSE](https://gitee.com/chinasoft_ohos/matomo-sdk-ohos/blob/master/LICENSE).